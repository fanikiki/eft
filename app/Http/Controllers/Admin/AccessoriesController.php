<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Products;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Models\Categories;
use Illuminate\Support\Facades\Auth;
use Validator;

class AccessoriesController extends Controller
{
    public function index()
    {
        $data = Products::all();
        return view('admin.accessories.index')->with(compact('data'));
    }

    public function create()
    {
        $products               = Products::pluck('name','id')->toArray();
        //$categories             = Categories::pluck('name','id')->toArray();
        return view('admin.accessories.edit')->with(compact('categories', 'products'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            //'slug'          => 'required|unique:products'
        );

        $this->validate($request, $rules);
        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Products();
        } else {
            $data = Products::find($id);
        }

        $data->name              = $request->name;
        $data->created_at        = $request->date;
        //$data->slug              = $request->slug;
        $data->description       = $request->description;
        $data->description_short = $request->description_short;
        $data->meta_description  = $request->meta_description;
        $data->meta_keywords     = $request->meta_keywords;
        $data->title             = $request->title;
        $data->price             = $request->price;
       

        //SAVING PRODUCTS
        $data->save();

        $this->UpdatePhotos($request, $data->id);

        //categories
        if ($request->parent) {
            $data->parents()->sync($request->parent);
        }


        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect('admin/accessories');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data              = Products::find($id);
        //$categories        = Categories::pluck('name','id')->toArray();
     
        return view('admin.accessories.edit')->with(compact('data', 'categories', 'parents'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            //'slug'          => 'required|unique:products,id,{$id}'
        );
        $this->validate($request, $rules);
        return $this->save($request, $id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Products::find($id);
        $product->parents()->detach();
        Products::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }
}
