<?php

namespace App\Http\Controllers\admin;

use App\Models\Categories;
use App\Models\Parameters;
use App\Models\ParametersProducts;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CategoriesController extends Controller
{
    public function index()
    {
        $data = Categories::orderBy('sort', 'asc')->get();
        return view('admin.categories.index')->with(compact('data'));
    }

    public function create()
    {
        /*return view('admin.categories.edit')->with($this->needs());*/
        return view('admin.categories.edit');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'slug'          => 'required|unique:categories'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Categories();
        } else {
            $data = Categories::find($id);
        }

        $data->name              = $request->name;
        $data->slug              = $request->slug;
        $data->description       = $request->description;
        $data->rent_days         = $request->rent_days;
        $data->created_at        = $request->date;
        /*$data->title             = $request->title;
        $data->meta_keywords     = $request->meta_keywords;
        $data->meta_description  = $request->meta_description;
        $data->title             = $request->title;*/
        $data->top               = $request->top;
        $data->sort              = $request->sort;
        $data->save();

        $this->UpdatePhotos($request, $data->id);

        //categories
        /*if ($request->parent) {
            $data->parents()->sync($request->parent, true);
        } else {
            $data->parents()->detach();
        }*/

        //parameters
        /*if ($request->parameters) {
            $relations = [];
            foreach ($request->parameters_products as $key => $item) {
                if ($item == 0) {
                    $pp = new ParametersProducts();
                    $pp->parameters_id = $request->parameters[$key];
                } else {
                    $pp = ParametersProducts::find($item);
                }
                $pp->sort = $key;
                $pp->table = 'categories';
                $pp->save();
                $relations[] = $pp;
            }
            $data->parameters()->saveMany($relations);
        } else {
            $data->parameters()->delete();
        }*/

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect('admin/categories');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
	    $data    = Categories::find($id);
        /*$parents = $data->parents->pluck('id')->toArray();
        $parameters2 = $data->parameters()->with('parameter')->orderBy('sort')->get();
        return view('admin.categories.edit')->with(compact('data', 'parents', 'parameters2'))->with($this->needs());*/

        return view('admin.categories.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            'slug'          => 'required|unique:categories,id,{$id}'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Categories::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    /*private function needs()
    {
        $categories = Categories::pluck('name','id')->toArray();
        $parameters = Parameters::orderBy('name')->pluck('name','id')->toArray();
        return compact('categories', 'parameters');
    }*/
}
