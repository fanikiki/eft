<?php

namespace App\Http\Controllers\Admin;

use App\Models\Manufacturers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ManufacturersController extends Controller
{
    public function index()
    {
        $data = Manufacturers::orderBy('name')->get();
        return view('admin.manufacturers.index')->with(compact('data'));
    }

    public function create()
    {
        return view('admin.manufacturers.edit');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'slug'          => 'required|unique:manufacturers'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Manufacturers();
        }else{
            $data = Manufacturers::find($id);
        }

        $data->name              = $request->name;
        //$data->enabled           = ($request->enabled == 1) ? 1 : 0;
        $data->created_at        = $request->date;
        $data->slug              = $request->slug;
        /*$data->description       = $request->description;
        $data->meta_description  = $request->meta_description;
        $data->meta_keywords     = $request->meta_keywords;
        $data->title             = $request->title;*/
        $data->save();

        /*$this->UpdatePhotos($request, $data->id);*/
        
        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect('admin/manufacturers');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Manufacturers::find($id);
        return view('admin.manufacturers.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            'slug'          => 'required|unique:manufacturers,id,{$id}'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Manufacturers::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }
}
