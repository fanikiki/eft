<?php

namespace App\Http\Controllers\Admin;

use App\Models\TireServices;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Validator;

class TireServicesController extends Controller
{
    public function index()
    {
        $data     = TireServices::all();
        return view('admin.tireservices.index')->with(compact('data'));
    }

    public function create()
    {
        return view('admin.tireservices.edit');
    }

    public function store(Request $request)
    {
        $rules = [
            'name'          => 'required',
            'slug'          => 'required|unique:tire_services'
        ];

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id = null){
        // store
        if (!isset($id)) {
            $data = new TireServices();
        }else{
            $data = TireServices::find($id);
        }

        $data->name              = $request->name;
        $data->created_at        = $request->date;
        $data->slug              = $request->slug;
        $data->description       = $request->description;
        $data->description_short = $request->description_short;
        $data->phone	         = $request->phone;
        $data->map	         	 = $request->map;
        $data->working_hours     = $request->working_hours;
        $data->meta_description  = $request->meta_description;
        $data->meta_keywords     = $request->meta_keywords;
        $data->title             = $request->title;
        $data->save();

        $this->UpdatePhotos($request, $data->id);

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect('admin/tire-services');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data     = TireServices::find($id);
        return view('admin.tireservices.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'          => 'required',
            'slug'          => 'required|unique:tire_services,id,{$id}',
        ];

        $validator = Validator::make($request->all(), $rules);

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        TireServices::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

}
