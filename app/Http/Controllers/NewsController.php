<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Tags;
use App\Models\Types;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Products;

class NewsController extends Controller
{
    public function getNews($slug)
    {
        $news = News::where('slug',"=",$slug)->firstOrFail();
        return view('news.show')->with('news', $news);
    }
    
    public function getNewsList()
    {
        $data = News::with('photos')->orderBy('created_at', 'desc')->get();
		return view('news.newslist')->with(compact('data'));
    }
}
