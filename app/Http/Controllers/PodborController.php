<?php

namespace App\Http\Controllers;

use App\Models\Manufacturers;
use App\Models\Podbor;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PodborController extends Controller
{
    public function index(Request $request)
    {
        //для подбора
        $data['manufacturers'] = Manufacturers::withPhotos()->get();
        $data['radius_arr'] = Products::getRadiusArray(1);
        $data['width_arr']  = Products::getWidthArray(1);
        $data['height_arr'] = Products::getHeightArray(1);
        $data['auto_brands'] = Podbor::getAutoBrands();


        if (isset($request->type) && $request->type == 'auto') {
            $data['type'] = $request->type;
            $data['autoname'] = $request->v . ' ' . $request->c . ' ' . $request->m . ' ' . $request->y . 'г.';
            $data['podbor'] = Podbor::where('vendor', $request->v)
                ->where('car', $request->c)
                ->where('year', $request->y)
                ->where('modification', $request->m)
                ->first();
        }

        return view('podbor')->with($data);
    }

    public function getTypesizes()
    {
        return [1 => '1', 2 => '2'];
    }

    public function getVendorCars()
    {
        $vendor  = Input::get("vendor");

        $list    = Podbor::select('car')
            ->where('vendor', $vendor)
            ->groupBy('car')
            ->orderBy('car')
            ->pluck('car')
            ->toArray();

        return response()->json($list);
    }

    public function getCarYears()
    {
        $vendor  = Input::get("vendor");
        $car  = Input::get("car");

        $list    = Podbor::select('year')
            ->where('vendor', $vendor)
            ->where('car', $car)
            ->groupBy('year')
            ->orderBy('year')
            ->pluck('year')
            ->toArray();

        return response()->json($list);
    }

    public function getYearModification()
    {
        $vendor  = Input::get("vendor");
        $car  = Input::get("car");
        $year  = Input::get("year");

        $list    = Podbor::select('modification')
            ->where('vendor', $vendor)
            ->where('car', $car)
            ->where('year', $year)
            ->groupBy('modification')
            ->orderBy('modification')
            ->pluck('modification')
            ->toArray();

        return response()->json($list);
    }
}
