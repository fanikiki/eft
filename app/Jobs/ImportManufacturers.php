<?php

namespace App\Jobs;

use App\Models\Manufacturers;
use App\Models\Models;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class ImportManufacturers implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dblist = DB::connection('mysql_import')
            ->table('shiny_29')
            ->select(DB::raw('distinct(series) as name'))
            ->orderBy('name')
            ->get();

        //deleting all old manufacturers
        //Manufacturers::truncate();

        foreach ($dblist as $item) {
            if ($item->name == "") continue;
            $slug = str_slug($item->name);
            $manufacturer = Manufacturers::firstOrNew(['slug' => $slug]);
            $manufacturer->name = ['ru' => $item->name];
            $manufacturer->slug = str_slug($item->name);
            $manufacturer->save();
        }

        echo $dblist->count() . " items DONE";
    }
}
