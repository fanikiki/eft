<?php

namespace App\Jobs;

use App\Models\Manufacturers;
use App\Models\Models;
use App\Models\ParametersProducts;
use App\Models\Photos;
use App\Models\Products;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use SebastianBergmann\Environment\Console;


class ImportTyresModels implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $task = 'models';

        switch ($task) {
            case 'models':
                $this->importModels();
                break;
            case 'typesizes':
                //TODO импортнуть без моделей
                break;
            case 'add-brand':
                //TODO у моделей добавить бренд исходя из бренда типоразмеров, если отсутствует
                break;
            case 'add-photo':
                //TODO если нет фотки source поменять на img_small у моделек
                break;
            default:
                echo 'no-task';
        }
    }

    private function importModels()
    {
        $manufacturers = Manufacturers::pluck('id', 'slug')->toArray();

        $dblist = DB::connection('mysql_import')
            ->table('shiny_29')
            ->select()
            ->orderBy('id')
            //->limit(100)
            ->where('reserve', 0)
            ->get();

        $models  = new Collection();
        $counter = 0;
        foreach ($dblist as $item) {
            $value = $this->getIntValue($item->shirina_profilya);
            if ($value == "" || $value >= 450) {
                $model_name = $item->model;
                $model_cut = preg_replace('/[\x00-\x1F\x7F\xA0]/u', '', $model_name);
                $model_cut = trim(str_replace($item->series, '', $model_cut));

                $typesizes = $this->getProducts($item->id, $model_cut, $item->series);

                if ($typesizes->count() == 0) {
                    DB::connection('mysql_import')
                        ->table('shiny_29')
                        ->where('id', $item->id)
                        ->update(['reserve' => 2]);
                    continue;
                }

                //dd($typesizes);

                DB::beginTransaction();

                //if no manufacturer in model
                if (!isset($item->series) || $item->series == '') {
                    $manufacturer = $typesizes{0}->series;
                } else {
                    $manufacturer = $item->series;
                }

                $model = new Models();
                $model->name = $model_cut;
                $model->slug = str_slug($model_cut);
                $man_slug = str_slug($manufacturer);
                $model->manufacturer_id = (isset($manufacturers[$man_slug]) ? $manufacturers[$man_slug] : 0);
                $model->category_id = 1;
                $model->reserve = $item->id;
                $model->save();


                //add products to model
                foreach ($typesizes as $typesize) {
                    $this->addProduct($typesize, $model->id);
                }

                //model photos
                $photo  = new Photos();
                $photo->source = $item->imgbig;
                $photo->img_small = $item->imgsmall;
                $photo->table = 'models';
                $photo->table_id = $model->id;
                $photo->save();


                DB::connection('mysql_import')
                    ->table('shiny_29')
                    ->where('id', $item->id)
                    ->update(['reserve' => 1]);

                DB::commit();

                echo ++$counter . '. ' . $model->name . PHP_EOL;
            }
        }

        //dd($models);
    }

    private function getProducts($id, $name, $series)
    {
        $query = DB::connection('mysql_import')
            ->table('shiny_29')
            ->select()
            ->orderBy('id')
            ->where('model', 'like', '%' . $name . '%')
            ->where('id', '<>', $id);

        if ($series != "") {
            $query->where('series', $series);
        }

        $dblist = $query->get();
        return $dblist;
    }

    private function addProduct($typesize, $model_id)
    {
        $product = new Products();
        $product->radius = $this->getIntValue($typesize->diametr);
        $product->width  = $this->getIntValue($typesize->shirina_profilya);
        $product->height = $this->getIntValue($typesize->visota_profilya);
        $product->vehicle_type = $this->getVehicleType($typesize->tip_avtomobilya);
        $product->reserve = $typesize->id;
        $product->model_id = $model_id;
        $product->save();

        //photos
        $photo  = new Photos();
        $photo->source = $typesize->imgbig;
        $photo->img_small = $typesize->imgsmall;
        $photo->table = 'products';
        $photo->table_id = $product->id;
        $photo->save();

        //parameters
        //Сезонность
        $value = trim($typesize->sezonnostj);
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 1;
            $pp->values_id = $this->getSeason($value);
            $pp->save();
        }

        //Индекс скорости
        $value = trim($typesize->indeks_skorosti);
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 2;
            $pp->value = $value;
            $pp->save();
        }

        //Индекс нагрузки
        $value = trim($typesize->indeks_nagruzki);
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 3;
            $pp->value = $value;
            $pp->save();
        }

        //Способ герметизации
        $value = $typesize->sposob_germetizatsii;
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 4;
            $pp->values_id = $this->getHermetization($value);
            $pp->save();
        }

        //Конструкция
        $value = $typesize->konstruktsiya;
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 5;
            $pp->values_id = $this->getConstruction($value);
            $pp->save();
        }

        //Технология RunFlat
        $value = $typesize->tehnologiya_runflat;
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 6;
            $pp->values_id = $this->getRunFlat($value);
            $pp->save();
        }

        //Шипы
        $value = $typesize->shipi;
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 7;
            $pp->values_id = $this->getShipi($value);
            $pp->save();
        }

        //Индекс максимальной скорости
        $value = trim($typesize->indeks_maksimaljnoy_skorosti);
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 8;
            $pp->value = $value;
            $pp->save();
        }

        //Максимальная накрузка на одну шину
        $value = trim($typesize->maksimaljnaya_nagruzka_na_odnu_shinu);
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 9;
            $pp->value = $value;
            $pp->save();
        }

        //Дополнительная информация
        $value = trim($typesize->dopolniteljnaya_informatsiya);
        if (isset($value) && $value != '') {
            $pp = new ParametersProducts();
            $pp->table = 'products';
            $pp->table_id = $product->id;
            $pp->parameters_id = 10;
            $pp->value = $value;
            $pp->save();
        }

        //reserve => 1
        DB::connection('mysql_import')
            ->table('shiny_29')
            ->where('id', $typesize->id)
            ->update(['reserve' => 1]);

    }

    private function getVehicleType($dbtype)
    {
        if (str_contains($dbtype, 'внедорожник'))
            return 1;

        return 0;
    }

    private function getSeason($value)
    {
        if (str_contains($value, 'летние')) {
            return 3;
        }

        if (str_contains($value, 'зимние')) {
            return 2;
        }
        //всесезонные
        return 1;
    }

    private function getHermetization($value)
    {
        if (str_contains($value, 'бескамерные')) {
            return 4;
        }

        //камерные
        return 5;
    }

    private function getConstruction($value)
    {
        if (str_contains($value, 'диагональные')) {
            return 6;
        }

        //радиальные
        return 7;
    }

    private function getRunFlat($value)
    {
        if (str_contains($value, 'нет')) {
            return 9;
        }

        if (str_contains($value, 'есть')) {
            return 8;
        }

        //опционально
        return 10;
    }

    private function getShipi($value)
    {
        if (str_contains($value, 'нет')) {
            return 12;
        }

        if (str_contains($value, 'есть')) {
            return 11;
        }

        //опционально
        return 13;
    }

    private function getIntValue($value)
    {
        return (int) preg_replace('/[^0-9]/', '', $value);
    }
}
