<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Jenssegers\Date\Date;

class BaseModel extends Model
{
    protected   $dateFormat = 'd-m-Y';

    public function getCreatedAtAttribute($date)
    {
        return Date::parse($date)->format($this->dateFormat);
    }

    public function setCreatedAtAttribute($date)
    {
        $this->attributes['created_at'] = Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    /**
     * Create the slug from the title
     */
    public function setSlugAttribute($value)
    {
        if ($value == "") {
            // grab the title and slugify it
            $this->attributes['slug'] = str_slug($this->name);
        }else{
            $this->attributes['slug'] = str_slug($value);
        }
    }
    
    public function setMetaDescriptionAttribute($values)
    {
        $this->saveMeta($values, "meta_description");
    }

    public function setMetaKeywordsAttribute($values)
    {
        $this->saveMeta($values, "meta_keywords");
    }

    public function setTitleAttribute($values)
    {
        $this->saveMeta($values, "title");
    }



    public function setNameAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['name'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['name'] = $value;
                    continue;
                }
                $this->attributes['name_' . $key] = $value;
            }
        }
    }

    public function setDescriptionAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['description'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['description'] = $value;
                    continue;
                }
                $this->attributes['description_' . $key] = $value;
            }
        }
    }

    public function setDescriptionShortAttribute($values) {

        if (!is_array($values)) {
            $this->attributes['description'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['description_short'] = $value;
                    continue;
                }
                $this->attributes['description_short_' . $key] = $value;
            }
        }
    }

    public function setUpdatedAtAttribute($value)
    {
        $this->attributes['updated_at'] = Carbon::now();
    }

    public function getUpdatedAtAttribute($date)
    {
        return Date::parse($date)->format($this->dateFormat);
    }

    public function getDescriptionAttribute()
    {
        $locale = Lang::locale();
        if ($locale == "ru"){
            return $this->attributes['description'];
        }else{
            return $this->attributes['description_' . $locale];
        }
    }

    public function getDescriptionShortAttribute()
    {
        $locale = Lang::locale();
        if ($locale == "ru"){
            return $this->attributes['description_short'];
        }else{
            return $this->attributes['description_short_' . $locale];
        }
    }

    public function getNameAttribute()
    {
        $locale = Lang::locale();
        if ($locale == "ru"){
            return $this->attributes['name'];
        }else{
            return $this->attributes['name_' . $locale];
        }
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photos','table_id')->where('table', $this->getTable())->orderBy('sort');
    }

    public function meta()
    {
        return $this->hasOne('App\Models\Meta','table_id')->where('table', $this->getTable());
    }

    public function scopeEnabled($query)
    {
        return $query->where('enabled', 1);
    }

    public function scopeDisabled($query)
    {
        return $query->where('enabled', 0);
    }

    public function scopeTop($query)
    {
        return $query->where('top', 1);
    }

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name",    "LIKE", "%$keyword%")
                    ->orWhere("name_ro", "LIKE", "%$keyword%")
                    ->orWhere("name_en", "LIKE", "%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%")
                    ->orWhere("description_ro", "LIKE", "%$keyword%")
                    ->orWhere("description_en", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    private function saveMeta($values, $type)
    {
        $table    = $this->getTable();
        //save model before saving meta
        if (!isset($this->id)) $this->save();
        $table_id = $this->id;

        $meta = Meta::where('table', $table)->where('table_id',$table_id)->first();
        if (!isset($meta)) {
            $meta = new Meta();
        }
        foreach($values as $key=>$value) {
            if ($key == "ru") {
                $meta->attributes[$type] = $value;
                continue;
            }
            $meta->attributes[$type . '_' . $key] = $value;
        }
        $meta->table            = $table;
        $meta->table_id         = $table_id;
        $meta->save();
    }

    public function mainphoto()
    {
        $fileName = "nophoto.png";
        if (isset($this->photos{0})) {
            $fileName = $this->photos{0}->source;
        }
        return $fileName;
    }

    public function parameters()
    {
        return $this->hasMany('App\Models\ParametersProducts', 'table_id')->where('table', $this->getTable())->orderBy('sort');
    }

        public function getTitle()
    {
        if (isset($this->meta->title) && ($this->meta->title != '') ) {
            return $this->meta->title;
        }
        return '';
    }

    public function getMetaKeywords()
    {
        if (isset($this->meta)) {
            return $this->meta->meta_keywords;
        }
        return '';
    }

    public function getMetaDescription()
    {
        if (isset($this->meta)) {
            return $this->meta->meta_description;
        }
        return '';
    }

}
