<?php

namespace App\Models;

class Manufacturers extends BaseModel
{
    protected $fillable = ['slug'];

    public function models()
    {
        return $this->hasMany('App\Models\Models', 'manufacturer_id');
    }

    public function scopeWithPhotos($query)
    {
        return $query->enabled()->has('photos')->with('photos')->orderBy('name');
    }
}
