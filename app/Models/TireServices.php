<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class TireServices extends BaseModel
{
    public function setPhoneAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['phone'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['phone'] = $value;
                    continue;
                }
                $this->attributes['phone_' . $key] = $value;
            }
        }
    }
    
    public function setWorkingHoursAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['working_hours'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['working_hours'] = $value;
                    continue;
                }
                $this->attributes['working_hours_' . $key] = $value;
            }
        }
    }
    
    public function getPhoneAttribute()
    {
        $locale = Lang::locale();
        if ($locale == "ru"){
            return $this->attributes['phone'];
        }else{
            return $this->attributes['phone_' . $locale];
        }
    }
    
    public function getWorkingHoursAttribute()
    {
        $locale = Lang::locale();
        if ($locale == "ru"){
            return $this->attributes['working_hours'];
        }else{
            return $this->attributes['working_hours_' . $locale];
        }
    }
    
    public function getMapAttribute()
	{
		return json_decode( $this->attributes['map'] );
	}

	public function setMapAttribute( $val )
	{
		$this->attributes['map'] = json_encode( $val );
	}
}
