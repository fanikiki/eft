<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 200);
            $table->string('name_ro', 200);
            $table->string('name_en', 200);
            $table->tinyInteger('type')->comment('0-input, 1-select');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parameters');
    }
}
