<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters_products', function(Blueprint $table) {
            $table->increments('id');
            $table->string('value');
            $table->string('value_ro');
            $table->string('value_en');
            $table->integer('parameters_id')->unsigned();
            $table->integer('values_id')->index();
            $table->integer('table_id')->index();
            $table->string('table', 20)->index();
            $table->integer('sort');
        });

        Schema::table('parameters_products', function(Blueprint $table) {
            $table->foreign('parameters_id')->references('id')->on('parameters')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameters_products', function(Blueprint $table) {
            $table->dropForeign('parameters_products_parameters_id_foreign');
        });
        
        Schema::drop('parameters_products');
    }
}
