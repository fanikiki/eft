-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 12 2017 г., 12:20
-- Версия сервера: 5.6.33-79.0
-- Версия PHP: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `u0081616_eft-arenda`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_ro` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_en` text COLLATE utf8_unicode_ci NOT NULL,
  `rent_days` int(11) NOT NULL DEFAULT '1',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `top` tinyint(1) NOT NULL,
  `views` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`, `name_ro`, `name_en`, `description`, `description_ro`, `description_en`, `description_short`, `description_short_ro`, `description_short_en`, `rent_days`, `enabled`, `top`, `views`, `sort`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Электронные тахеометры', '', '', '', '', '', '', '', '', 4, 1, 1, 0, 0, 'elektronnye-taxeometry', '2017-03-30 21:00:00', '2017-03-31 08:01:10'),
(2, 'Геодезические приемники', '', '', '', '', '', '', '', '', 3, 1, 1, 0, 0, 'geodezicheskie-priemniki', '2017-03-30 21:00:00', '2017-03-31 03:13:07');

-- --------------------------------------------------------

--
-- Структура таблицы `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_ro` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `content`
--

INSERT INTO `content` (`id`, `name`, `name_en`, `name_ro`, `description`, `description_en`, `description_ro`, `enabled`, `views`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Условия аренды', '', '', '<p>Условия аренды геодезических приборов в&nbsp;нашей компании не&nbsp;могут не&nbsp;порадовать вас, ведь мы&nbsp;постарались свести к&nbsp;минимуму все бюрократические элементы и&nbsp;временные затраты, сделав процесс заключения договоров необременительным и&nbsp;приятным. Обращаем внимание потенциальных арендаторов, что мы&nbsp;предоставляем в&nbsp;аренду геодезические приборы без залога, что является немаловажным плюсом нашей деятельности.</p>\r\n\r\n<p><strong>Для заключения договора аренды нам нужно:</strong></p>\r\n\r\n<p><em>Список необходимых документов:</em></p>\r\n\r\n<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Реквизиты</p>\r\n\r\n<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Устав компании;</p>\r\n\r\n<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Свидетельство ОГРН;</p>\r\n\r\n<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Свидетельство ИНН;</p>\r\n\r\n<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Выписка из ЕГРЮЛ;</p>\r\n\r\n<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Уведомление об упрощенной системе налогообложения (если есть);</p>\r\n\r\n<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Протокол и приказ о полномочиях руководителя (директора, генерального директора, президента);</p>\r\n', '', '', 1, 0, 'usloviya-arendy', '2017-04-10 09:44:46', '2017-04-10 09:44:46'),
(2, 'О нас', '', '', '<p>Подразделение &laquo;EFT ARENDA&raquo;&nbsp;входит в группу компаний &laquo;<a href="http://www.eftgroup.ru/" target="_blank">EFT GROUP</a>&raquo;. Данное подразделение специализируется на предоставлении геодезического оборудования в аренду.</p>\r\n\r\n<p><strong>Наши преимущества:</strong></p>\r\n\r\n<ul>\r\n	<li>Самый большой парк арендного оборудования в&nbsp;России;</li>\r\n	<li>Минимум &laquo;бюрократии&raquo; Процедура оформления занимает минимум времени;</li>\r\n	<li>Аренда геодезического оборудования осуществляется без залога;</li>\r\n	<li>Мы&nbsp;специализируемся на&nbsp;аренде геодезического оборудования, что позволяет нам предложить лучшие цены на&nbsp;рынке;</li>\r\n	<li>Мы&nbsp;можем доставить выбранное оборудования в&nbsp;любую точку России, что позволит решить Вашу задачу более оперативно;</li>\r\n	<li>Компания гарантирует исправность оборудования;</li>\r\n	<li>Оборудование сертифицировано и прошло метрологическую аттестацию;</li>\r\n	<li>Мы хотим помогать Вам!</li>\r\n</ul>\r\n\r\n<p>&nbsp;К нам обращаются Заказчики с самыми разными предпочтениями. Одни желают воспользоваться только самым современным и новейшим оборудованием, включающим в себя все инновационные технологии, другие же, наоборот, доверяют лишь проверенным длительным временем геодезическим приборам и предпочитают менее современные, зато надежные и известные модели.</p>\r\n\r\n<p>Мы&nbsp;идем навстречу всем пожеланиям, и&nbsp;выполняем просьбы и&nbsp;условия всех наших арендаторов. Если вы&nbsp;сомневаетесь в&nbsp;целесообразности аренды оборудования или не&nbsp;знаете, что именно вам нужно, профессионалы нашей компании всегда выручат вас дельным советом и&nbsp;реальной помощью в&nbsp;подборе оборудования для геодезических работ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>У&nbsp;нас имеются приборы тахеометры, GNSS &mdash; приемники, цифровые нивелиры) практически всех ведущих мировых производителей: Trimble, Nikon, Sokkia, Topcon, Leica. Наши инструменты выдается полностью укомплектованным и&nbsp;готовым к&nbsp;использованию, все оборудование исправно, отлично работает, находится в&nbsp;соответствии с&nbsp;заявленными рабочими параметрами. Кроме того, все приборы проходят метрологическую аттестацию в&nbsp;ФБУ &laquo;РОСТЕСТ-МОСКВА&raquo;.</p>\r\n\r\n<p>Также мы&nbsp;рады поставить в&nbsp;известность своих будущих клиентов, что наша компания &laquo;EFT ARENDA&raquo; располагает наиболее обширным ассортиментом геодезических инструментов и&nbsp;приборов в&nbsp;нашей стране. У&nbsp;нас вы&nbsp;найдете&nbsp;то, чего можете не&nbsp;найти в&nbsp;других компаниях.</p>\r\n\r\n<p>Мы&nbsp;можем доставить геодезическое оборудование в&nbsp;любую точку нашей страны (средний срок доставки 3&nbsp;дня), что делает аренду в&nbsp;компании &laquo;EFT ARENDA&raquo; наиболее оптимальным с&nbsp;точки зрения соотношения цены и&nbsp;качества.</p>\r\n\r\n<p>Заказать аренду геодезических приборов&nbsp;Вы сможете по&nbsp;телефону&nbsp;&mdash;&nbsp;(495)&nbsp;212-1717.</p>\r\n\r\n<p><em>Наша задача&nbsp;&mdash; реальная помощь Вам!</em></p>\r\n', '', '', 1, 0, 'o-nas', '2017-04-10 09:44:08', '2017-04-10 09:44:08');

-- --------------------------------------------------------

--
-- Структура таблицы `lists`
--

CREATE TABLE IF NOT EXISTS `lists` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_ro` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_en` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `lists`
--

INSERT INTO `lists` (`id`, `name`, `name_ro`, `name_en`, `description`, `description_ro`, `description_en`, `description_short`, `description_short_ro`, `description_short_en`, `parent_id`, `enabled`, `views`, `sort`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'На главной', '', '', '', '', '', '', '', '', 0, 1, 0, 0, 'na-glavnoj', '2017-03-30 21:00:00', NULL),
(2, 'Заголовок, описание, картинка', '', '', '<p>Мы располагаем большим ассортиментом различного рода устройств разных производителей и моделей. Мы уверены, что вы найдете у нас оборудование, которое соответствует всем вашим требованиям.</p>\r\n', '', '', 'Аренда оборудования удобно, легко и просто', '', '', 1, 1, 0, 0, 'zagol', '2017-03-30 21:00:00', NULL),
(3, 'Парк оборудования', '', '', '', '', '', 'http://eft-arenda.ebav.ru/catalog', '', '', 1, 1, 0, 0, 'par', '2017-03-30 21:00:00', NULL),
(4, 'Мы предлагаем', '', '', '', '', '', '', '', '', 0, 1, 0, 0, 'my-pr', '2017-03-30 21:00:00', NULL),
(5, 'Работаем без залога', '', '', '<p>(минимум документов)</p>\r\n', '', '', '', '', '', 4, 1, 0, 0, 'ar', '2017-03-30 21:00:00', NULL),
(6, 'Доставка в любую точку России', '', '', '<p>(быстро и надежно)</p>\r\n', '', '', '', '', '', 4, 1, 0, 0, 'arenda', '2017-03-30 21:00:00', NULL),
(7, 'Самый большой парк', '', '', '<p>(всё оборудование поверено и исправлено)</p>\r\n', '', '', '', '', '', 4, 1, 0, 0, 'kachestvo-prevyshe-vsego', '2017-03-30 21:00:00', NULL),
(8, 'Стоимость аренды оборудования', '', '', '', '', '', '', '', '', 0, 1, 0, 0, 's', '2017-04-09 21:00:00', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_ro` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_en` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `reserve` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `name`, `name_ro`, `name_en`, `description`, `description_ro`, `description_en`, `description_short`, `description_short_ro`, `description_short_en`, `enabled`, `slug`, `reserve`, `created_at`, `updated_at`) VALUES
(1, 'Leica', '', '', '', '', '', '', '', '', 1, 'leica', '', '2017-03-31 02:40:32', '2017-03-31 02:40:32'),
(2, 'EFT', '', '', '', '', '', '', '', '', 1, 'eft', '', '2017-03-31 03:13:29', '2017-03-31 03:13:29'),
(3, 'Nikon', '', '', '', '', '', '', '', '', 1, 'nikon', '', '2017-03-31 03:18:15', '2017-03-31 03:18:15'),
(4, 'Trimble', '', '', '', '', '', '', '', '', 1, 'trimble', '', '2017-03-31 03:19:47', '2017-03-31 03:19:47');

-- --------------------------------------------------------

--
-- Структура таблицы `meta`
--

CREATE TABLE IF NOT EXISTS `meta` (
  `id` int(10) unsigned NOT NULL,
  `meta_description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description_ro` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords_ro` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `title_ro` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `table_id` int(11) NOT NULL,
  `table` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `meta`
--

INSERT INTO `meta` (`id`, `meta_description`, `meta_description_ro`, `meta_description_en`, `meta_keywords`, `meta_keywords_ro`, `meta_keywords_en`, `title`, `title_ro`, `title_en`, `table_id`, `table`) VALUES
(1, '', '', '', '', '', '', '', '', '', 1, 'models'),
(2, '', '', '', '', '', '', '', '', '', 2, 'models'),
(3, '', '', '', '', '', '', '', '', '', 3, 'models'),
(4, '', '', '', '', '', '', '', '', '', 4, 'models'),
(5, '', '', '', '', '', '', '', '', '', 5, 'models'),
(6, '', '', '', '', '', '', '', '', '', 6, 'models'),
(7, '', '', '', '', '', '', '', '', '', 7, 'models'),
(8, '', '', '', '', '', '', '', '', '', 8, 'models'),
(9, '', '', '', '', '', '', '', '', '', 1, 'content'),
(10, '', '', '', '', '', '', '', '', '', 2, 'content');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(41, '2014_10_12_000000_create_users_table', 1),
(42, '2014_10_12_100000_create_password_resets_table', 1),
(43, '2016_04_08_084345_create_content_table', 1),
(44, '2016_04_28_122305_create_photos_table', 1),
(45, '2016_05_25_092458_create_products', 1),
(46, '2016_05_25_111243_create_meta', 1),
(47, '2016_05_25_115447_create_categories', 1),
(48, '2016_06_23_074721_products_categories', 1),
(49, '2017_02_02_151134_create_lists', 1),
(50, '2017_02_15_115409_create_parameters', 1),
(51, '2017_02_15_120117_create_parameters_products', 1),
(52, '2017_02_17_092235_create_manufacturers', 1),
(53, '2017_02_17_092415_create_models', 1),
(54, '2017_02_18_100602_create_parameters_values', 1),
(55, '2014_10_12_000000_create_users_table', 1),
(56, '2014_10_12_100000_create_password_resets_table', 1),
(57, '2016_04_08_084345_create_content_table', 1),
(58, '2016_04_28_122305_create_photos_table', 1),
(59, '2016_05_25_092458_create_products', 1),
(60, '2016_05_25_111243_create_meta', 1),
(61, '2016_05_25_115447_create_categories', 1),
(62, '2016_06_23_074721_products_categories', 1),
(63, '2017_02_02_151134_create_lists', 1),
(64, '2017_02_15_115409_create_parameters', 1),
(65, '2017_02_15_120117_create_parameters_products', 1),
(66, '2017_02_17_092235_create_manufacturers', 1),
(67, '2017_02_17_092415_create_models', 1),
(68, '2017_02_18_100602_create_parameters_values', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `models`
--

CREATE TABLE IF NOT EXISTS `models` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_ro` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_en` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturer_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `reserve` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `models`
--

INSERT INTO `models` (`id`, `name`, `name_ro`, `name_en`, `description`, `description_ro`, `description_en`, `description_short`, `description_short_ro`, `description_short_en`, `price`, `enabled`, `slug`, `manufacturer_id`, `category_id`, `reserve`, `created_at`, `updated_at`) VALUES
(1, 'Базовый комплект ГЛОНАСС/GPS-приемника EFT М1', '', '', '<p><strong>Общие характеристики:</strong></p>\r\n\r\n<p>&bull; 220 каналов<br />\r\n&nbsp; - GPS: L1 C/A, L2E, L2C, L5<br />\r\n&nbsp; - ГЛОНАСС: L1 C/A, L1 P, L2 C/A и L2P<br />\r\n&nbsp; - Galileo: L1, E5A, E5B, E5AltBOC<br />\r\n&nbsp; - SBAS: L1 C/A, L5<br />\r\n&nbsp; - Beidou: B1, B2<br />\r\n&nbsp; - QZSS: L1, L2C, L5<br />\r\n&bull; Высокоточный множественный коррелятор измерений псевдодальностей GNSS<br />\r\n&bull; Измерения фаз несущих частот GNSS с очень низким уровнем шумов и точностью &lt;1 мм в полосе частот 1 Гц<br />\r\n&bull; Проверенная технология Trimble для отслеживания спутников с малыми углами возвышения<br />\r\n&bull; Время инициализации &lt; 10 сек<br />\r\n&bull; Надежность инициализации &gt; 99,9%</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Точность измерений:</strong></p>\r\n\r\n<p>&bull; Статика и Быстрая статика:<br />\r\n&nbsp; - В плане: 2,5 мм +0,5мм/км<br />\r\n&nbsp; - По высоте: 5 мм +0,5мм/км<br />\r\n&bull; Кинематика с постобработкой:<br />\r\n&nbsp; - В плане: 10 мм +1мм/км<br />\r\n&nbsp; - По высоте: 25 мм +1мм/км<br />\r\n&bull; Кинематика в реальном времени (RTK):<br />\r\n&nbsp; - В плане: 8 мм +1мм/км<br />\r\n&nbsp; - По высоте: 15 мм +1мм/км<br />\r\n&bull; Дифференциальные кодовые измерения (DGNSS):<br />\r\n&nbsp; - В плане: 25см +1мм/км<br />\r\n&nbsp; - По высоте: 50см +1мм/км<br />\r\n&bull; Дифференциальные кодовые измерения SBAS:<br />\r\n&nbsp; - В плане: 50см&nbsp;<br />\r\n&nbsp; - По высоте: 85см</p>\r\n\r\n<p><strong>Аппаратурные характеристики:</strong></p>\r\n\r\n<p>&bull; Размеры (ШxВ): 19,5 см х 10,4 см<br />\r\n&bull; Вес (с аккумулятором): 1,3 кг<br />\r\n&bull; Рабочая температура: от -45&deg;С до +65&deg;С<br />\r\n&bull; Температура хранения: от -55&deg;С до +85&deg;С<br />\r\n&bull; Пыле- и влагозащищенность: IP67</p>\r\n', '', '', '', '', '', 900, 1, 'eft-m1-gnss', 2, 2, '', '2017-04-10 04:38:56', '2017-04-10 04:38:56');

-- --------------------------------------------------------

--
-- Структура таблицы `parameters`
--

CREATE TABLE IF NOT EXISTS `parameters` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_ro` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0-input, 1-select'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `parameters_products`
--

CREATE TABLE IF NOT EXISTS `parameters_products` (
  `id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value_ro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parameters_id` int(10) unsigned NOT NULL,
  `values_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `table` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `parameters_values`
--

CREATE TABLE IF NOT EXISTS `parameters_values` (
  `id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value_ro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `parameters_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL,
  `source` text COLLATE utf8_unicode_ci NOT NULL,
  `img_small` text COLLATE utf8_unicode_ci NOT NULL,
  `table_id` int(11) NOT NULL,
  `table` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `photos`
--

INSERT INTO `photos` (`id`, `source`, `img_small`, `table_id`, `table`, `token`, `sort`) VALUES
(10, '58de04bd37e27.png', '', 1, 'products', '', 10),
(11, '58de095785b90.png', '', 2, 'products', '', 11),
(13, 'zagolovok-opisanie-kartinka_13.jpg', '', 3, 'lists', '', 13),
(31, 'eft-m1-gnss_31.jpg', '', 1, 'models', '', 31),
(32, 'eft-m1-gnss_32.jpg', '', 1, 'models', '', 32),
(33, 'eft-m1-gnss_33.jpg', '', 1, 'models', '', 33),
(34, 'zagol_34.jpg', '', 2, 'lists', '', 34),
(35, 'ar_35.jpg', '', 5, 'lists', '', 35),
(36, 'arenda_36.jpg', '', 6, 'lists', '', 36),
(37, 'kachestvo-prevyshe-vsego_37.jpg', '', 7, 'lists', '', 37),
(38, 's_38.png', '', 8, 'lists', '', 38);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_ro` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_ro` text COLLATE utf8_unicode_ci NOT NULL,
  `description_short_en` text COLLATE utf8_unicode_ci NOT NULL,
  `price` double(15,2) NOT NULL,
  `sale_price` double(15,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `top` tinyint(1) NOT NULL,
  `views` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `radius` double(8,2) NOT NULL,
  `width` double(8,2) NOT NULL,
  `height` double(8,2) NOT NULL,
  `vehicle_type` tinyint(4) NOT NULL COMMENT '0-легковой, 1-внедорожник',
  `model_id` int(11) NOT NULL,
  `img_100x200` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `imgs` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reserve` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `name`, `name_ro`, `name_en`, `description`, `description_ro`, `description_en`, `description_short`, `description_short_ro`, `description_short_en`, `price`, `sale_price`, `enabled`, `top`, `views`, `quantity`, `product_number`, `radius`, `width`, `height`, `vehicle_type`, `model_id`, `img_100x200`, `imgs`, `reserve`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '<p><strong>ПЕРЕДОВЫЕ ТЕХНОЛОГИИ</strong><br />\nВ основе приемника EFT M1&nbsp;GNSS лежит высоко&shy;технологичная GNSS-плата Trimble BD970 с технологией Trimble Maxwell 6 для отслеживания данных со спутников на 220 каналах. Благодаря этому приемник поддерживает широкий спектр спутниковых сигналов: GPS NAVSTAR, включая L2C и L5, ГЛОНАСС, Galileo и различные SBAS. В EFT M1&nbsp;GNSS максимально реализованы технологии Trimble для точного и надежного позиционирования в сложных условиях с ограниченной видимостью небосвода и в условиях с большой многолучевостью.<br />\n&nbsp;</p>\n', '', '', '', '', '', 500.00, 0.00, 1, 0, 0, 0, '11111', 0.00, 0.00, 0.00, 0, 1, '', '', '', '2017-04-04 08:16:31', '0000-00-00 00:00:00'),
(2, '', '', '', '<p><strong>ЛЕГКАЯ НАСТРОЙКА</strong><br />\nПанель управления EFT M1&nbsp;GNSS позволяет не только изменять параметры GNSS-наблюдений: маску возвышения и интервал записи, но и выбирать режим работы. Таким образом, большинство работ можно выполнять вообще без полевого контроллера. При этом отсутствует необходимость повторных настроек приемника, так как EFT M1&nbsp;GNSS запоминает последний выбранный режим. Для повторного запуска достаточно включить питание.</p>\n', '', '', '', '', '', 600.00, 0.00, 1, 0, 0, 0, '2222', 0.00, 0.00, 0.00, 0, 1, '', '', '', '2017-04-04 08:16:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `products_categories`
--

CREATE TABLE IF NOT EXISTS `products_categories` (
  `id` int(10) unsigned NOT NULL,
  `products_id` int(10) unsigned NOT NULL,
  `categories_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `open_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `top` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `rights` tinyint(4) NOT NULL,
  `sale` double(5,2) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `open_password`, `mobile`, `comment`, `top`, `enabled`, `rights`, `sale`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Xsort', 'support@xsort.md', '$2y$10$PaGZo3Ce8WQBu2BcYCn6Nuqp3eWPFIxWFk/p2wKDytjYT/k4M2uii', '', '', '', 0, 0, 1, 0.00, NULL, NULL, NULL),
(2, 'arenda', 'info@eft-arenda.ru', '$2y$10$zsxnTd/.lVG3NjH.wTGdtuDJr8Qi0gVFuWhTmgY7LY2ZVWfeLNTMy', '', '', '', 0, 0, 1, 0.00, NULL, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Индексы таблицы `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_slug_unique` (`slug`);

--
-- Индексы таблицы `lists`
--
ALTER TABLE `lists`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lists_slug_unique` (`slug`),
  ADD KEY `lists_parent_id_index` (`parent_id`);

--
-- Индексы таблицы `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `manufacturers_slug_unique` (`slug`);

--
-- Индексы таблицы `meta`
--
ALTER TABLE `meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_table_id_index` (`table_id`),
  ADD KEY `meta_table_index` (`table`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`),
  ADD KEY `models_slug_index` (`slug`);

--
-- Индексы таблицы `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `parameters_products`
--
ALTER TABLE `parameters_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parameters_products_values_id_index` (`values_id`),
  ADD KEY `parameters_products_table_id_index` (`table_id`),
  ADD KEY `parameters_products_table_index` (`table`),
  ADD KEY `parameters_products_parameters_id_foreign` (`parameters_id`);

--
-- Индексы таблицы `parameters_values`
--
ALTER TABLE `parameters_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parameters_values_parameters_id_foreign` (`parameters_id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photos_table_id_index` (`table_id`),
  ADD KEY `photos_table_index` (`table`),
  ADD KEY `photos_token_index` (`token`),
  ADD KEY `idx_sort` (`sort`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_product_number_index` (`product_number`),
  ADD KEY `products_radius_index` (`radius`),
  ADD KEY `products_width_index` (`width`),
  ADD KEY `products_height_index` (`height`),
  ADD KEY `products_vehicle_type_index` (`vehicle_type`),
  ADD KEY `products_model_id_index` (`model_id`);

--
-- Индексы таблицы `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_categories_products_id_foreign` (`products_id`),
  ADD KEY `products_categories_categories_id_foreign` (`categories_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `content`
--
ALTER TABLE `content`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `lists`
--
ALTER TABLE `lists`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `meta`
--
ALTER TABLE `meta`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT для таблицы `models`
--
ALTER TABLE `models`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `parameters`
--
ALTER TABLE `parameters`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `parameters_products`
--
ALTER TABLE `parameters_products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `parameters_values`
--
ALTER TABLE `parameters_values`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `parameters_products`
--
ALTER TABLE `parameters_products`
  ADD CONSTRAINT `parameters_products_parameters_id_foreign` FOREIGN KEY (`parameters_id`) REFERENCES `parameters` (`id`);

--
-- Ограничения внешнего ключа таблицы `parameters_values`
--
ALTER TABLE `parameters_values`
  ADD CONSTRAINT `parameters_values_parameters_id_foreign` FOREIGN KEY (`parameters_id`) REFERENCES `parameters` (`id`);

--
-- Ограничения внешнего ключа таблицы `products_categories`
--
ALTER TABLE `products_categories`
  ADD CONSTRAINT `products_categories_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `products_categories_products_id_foreign` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
