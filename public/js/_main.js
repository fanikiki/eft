/**
 * Created by Daniel on 009 09.02.17.
 */
$("#main-slider").owlCarousel({
    items: 1,
    dots: false,
    nav: false,
    mouseDrag: false
});

$(function () {
    var dtpickerfrom = $('.datepicker-from');
    var dtpickerto = $('.datepicker-to');

    dtpickerfrom.datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY',
        minDate: moment(),
        widgetPositioning: {
            vertical: 'top'
        }
    });

    dtpickerto.datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY',
        minDate: moment().add('3', 'days'),
        widgetPositioning: {
            vertical: 'top'
        }
    });

    /*dtpickerfrom.on('dp.change', function (e) {
        dtpickerto.data('DateTimePicker').options({minDate: e.date.add('3', 'days')});
    });

    dtpickerto.on('dp.change', function (e) {
        dtpickerfrom.data('DateTimePicker').options({maxDate: e.date.subtract('3', 'days')});
    });*/
});

/*var swiperProductTah = $('.swiper--product-tah .swiper-container');
var swiperProductCon = $('.swiper--product-con .swiper-container');
var swiperProductNiv = $('.swiper--product-niv .swiper-container');*/

/* категории продуктов */
/*$(document).ready(function () {
    if ($().swiper) {
        if (swiperProductTah.length) {
            swiperProductTah = new Swiper(swiperProductTah, {
                direction: 'horizontal',
                slidesPerView: 4,
                spaceBetween: 30,
                //autoplay: 2000,
                loop: false,
                paginationClickable: true,
                pagination: '.swiper-pagination',
                nextButton: '.swiper-button-next.tah',
                prevButton: '.swiper-button-prev.tah'
            });
        }
        if (swiperProductCon.length) {
            swiperProductCon = new Swiper(swiperProductCon, {
                direction: 'horizontal',
                slidesPerView: 4,
                spaceBetween: 30,
                //autoplay: 2000,
                loop: true,
                paginationClickable: true,
                pagination: '.swiper-pagination',
                nextButton: '.swiper-button-next.con',
                prevButton: '.swiper-button-prev.con'
            });
        }
        if (swiperProductNiv.length) {
            swiperProductNiv = new Swiper(swiperProductNiv, {
                direction: 'horizontal',
                slidesPerView: 4,
                spaceBetween: 30,
                //autoplay: 2000,
                loop: true,
                paginationClickable: true,
                pagination: '.swiper-pagination',
                nextButton: '.swiper-button-next.niv',
                prevButton: '.swiper-button-prev.niv'
            });
        }
    }
});

function updater() {
    if ($().swiper) {
        if (typeof (swiperProductTah.length) == 'undefined') {
            swiperProductTah.update();
            swiperProductTah.onResize();
            if ($(window).width() > 991) {
                swiperProductTah.params.slidesPerView = 4;
                //swiperOffersBest.stopAutoplay();
                //swiperOffersBest.startAutoplay();
            }
            else {
                if ($(window).width() < 768) {
                    swiperProductTah.params.slidesPerView = 1;
                    //swiperOffersBest.stopAutoplay();
                    //swiperOffersBest.startAutoplay();
                }
                else {
                    swiperProductTah.params.slidesPerView = 2;
                    //swiperOffersBest.stopAutoplay();
                    //swiperOffersBest.startAutoplay();
                }
            }
        }
        if (typeof (swiperProductCon.length) == 'undefined') {
            swiperProductCon.update();
            swiperProductCon.onResize();
            if ($(window).width() > 991) {
                swiperProductCon.params.slidesPerView = 4;
                //swiperOffersBest.stopAutoplay();
                //swiperOffersBest.startAutoplay();
            }
            else {
                if ($(window).width() < 768) {
                    swiperProductCon.params.slidesPerView = 1;
                    //swiperOffersBest.stopAutoplay();
                    //swiperOffersBest.startAutoplay();
                }
                else {
                    swiperProductCon.params.slidesPerView = 2;
                    //swiperOffersBest.stopAutoplay();
                    //swiperOffersBest.startAutoplay();
                }
            }
        }
        if (typeof (swiperProductNiv.length) == 'undefined') {
            swiperProductNiv.update();
            swiperProductNiv.onResize();
            if ($(window).width() > 991) {
                swiperProductNiv.params.slidesPerView = 4;
                //swiperOffersBest.stopAutoplay();
                //swiperOffersBest.startAutoplay();
            }
            else {
                if ($(window).width() < 768) {
                    swiperProductNiv.params.slidesPerView = 1;
                    //swiperOffersBest.stopAutoplay();
                    //swiperOffersBest.startAutoplay();
                }
                else {
                    swiperProductNiv.params.slidesPerView = 2;
                    //swiperOffersBest.stopAutoplay();
                    //swiperOffersBest.startAutoplay();
                }
            }
        }
    }
}

$(window).resize(function () {
    // Refresh isotope
    if ($().isotope) {
        isotopeContainer.isotope('reLayout'); // layout/relayout on window resize
    }
    updater();
});*/

/*$("#package1, #package2, #package3").click(function () {
    $("#my-modal").modal();
});*/


$(document).on("click", ".productpackage", function () {
    var product_photo = $(this).data("photo");
    var product_price = $(this).data("price");
    var product_description = $(this).data("description");

    var product_modal = $("#product-modal");

    product_modal.find("#product-image").attr("src", "uploaded/" + product_photo);
    product_modal.find("#product-price").text(product_price);
    product_modal.find("#product-description").empty().append(product_description);

    product_modal.modal();
});

$(function () {
    /* Страница Контакты */
    /*$("ul#cities li a").each(functionclick(function () {
        $("#city-address").text(vologda[0].address);
        $("#city-phone").text(vologda[0].phone);

        $("ul#cities li").find(".active").removeClass("active");
        $(this).addClass("active");
    });*/

    $("ul.cities li a").each(function () {

        var city = $(this).attr('id');

        $(this).click(function () {
            $("#city-name").text($(this).text());
            $("#city-address").text(window[city].address);
            $("#city-phone").text(window[city].phone);
            $("#city-email").html(window[city].email);

            $("ul.cities li").find(".active").removeClass("active");
            $(this).addClass("active");
        });
    });
});

$(function () {

    var location_url = window.location.href.substr(window.location.href.lastIndexOf("/")+1);

    $("ul.nav.sf-menu li a").each(function (index, value) {

        var href = $(value).attr("href");
        var menu_url = href.substr(href.lastIndexOf("/")+1);

        if(location_url === menu_url){
            $("ul.nav.sf-menu").find("li").removeClass("active");
            $(this).parent().addClass("active");
        }
    });
});

//Валидация формы заказа (калькулятор)
$(function () {
    $("#frm_calc").submit(function (e) {
        e.preventDefault();
    }).validate({
        ignore: [],
        rules: {
            fio: "required",
            mobile: "required",
            email: "required",
            hiddenRecaptcha: {
                required: function () {
                    return grecaptcha.getResponse() == '';
                }
            }
        },
        errorPlacement: function(error,element) {
            if($(element).attr('id') == 'hiddenRecaptcha'){
                $(".g-recaptcha").find("div").eq(0).css("border", "1px solid red");
            }

            $(element).addClass('error');
        },
        success: function(element) {

            if($(element).attr('id') == 'hiddenRecaptcha'){
                $(".g-recaptcha").find("div").eq(0).css("border", "none");
            }

            $(element).parent().removeClass('error');
        },
        submitHandler: function () {

            $("#frm_calc").ajaxSubmit({
                data: {
                    calc_type: $("select#calc-type option:selected").val(),
                    calc_manufacturer: $("select#calc-manufacturer option:selected").val(),
                    calc_model: $("select#calc-model option:selected").val(),
                    calc_package: $("select#calc-package option:selected").val(),
                    quantity: $("input#quantity").val(),
                    date_from: $("#date_from").val(),
                    date_to: $("#date_to").val()
                }
            });
            $("#frm_calc").resetForm();
            $("#my-modal-order").modal('toggle');
            $("#order_success").modal();

            $(".g-recaptcha").find("div").eq(0).css("border", "none");
            grecaptcha.reset();
        }
    });

    //Валидация формы заказа (продукт)
    $("#frm_model").submit(function (e) {
        e.preventDefault();
    }).validate({
       ignore: [],
       rules:{
           fio: "required",
           phone: "required",
           email: "required",
           city: "required",
           company: "required",
           agree: "required",
           hiddenRecaptcha: {
               required: function () {
                   return grecaptcha.getResponse() == '';
               }
           }
       },
        errorPlacement: function(error,element) {
            $(element).addClass('error');

            if($(element).attr("id") === "agree"){
                $(element).parent().css("border", "1px solid red");
            }

            if($(element).attr('id') == 'hiddenRecaptcha'){
                $(".g-recaptcha").find("div").eq(0).css("border", "1px solid red");
            }
        },
        success: function(element) {
            $(element).parent().removeClass('error');
            $(element).parent().css("border", "none");

            if($(element).attr('id') == 'hiddenRecaptcha'){
                $(".g-recaptcha").find("div").eq(0).css("border", "none");
            }
        },
        submitHandler: function () {
            $("#frm_model").ajaxSubmit({
                data: {
                    model_slug: $("input[name=model_slug]").val(),
                    package_id: $("input[name=radio]:checked").data("package-id")
                }
            });
            $("#frm_model").resetForm();
            $("#agree").parent().css("border", "none");
            $("#order_success").modal();

            $(".g-recaptcha").find("div").eq(0).css("border", "none");
            grecaptcha.reset();
        }
    });
});

$("#btn_frm_model").click(function () {
    $(this).closest("form").submit();
});

$('.google-map').click(function () {
    $(this).find('iframe').addClass('clicked')
}).mouseleave(function () {
    $(this).find('iframe').removeClass('clicked')
});