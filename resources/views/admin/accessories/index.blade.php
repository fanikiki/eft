@extends('admin.common.list',
    [
        'title'       =>  'Расходники',
        'desc'        =>  'Список товаров',
        'model'       =>  'accessories',
        'fields'      =>  ['name' => 'Наименование', 'created_at' => 'Создан'],
        'data'        =>  $data
    ]
)