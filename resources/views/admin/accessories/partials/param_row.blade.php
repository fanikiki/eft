<div class="col-sm-9">
    {{ Form::text('params[' . $num . '][ru]', (isset($data->params) ? $data->params[$num]['ru'] : ''), ['class' => 'col-sm-11 col-xs-12']) }}
    {{ Form::text('params[' . $num . '][ro]', (isset($data->params) ? $data->params[$num]['ro'] : ''), ['class' => 'col-sm-11 col-xs-12 lang-ro', 'placeholder' => 'русский']) }}
    {{ Form::text('params[' . $num . '][en]', (isset($data->params) ? $data->params[$num]['en'] : ''), ['class' => 'col-sm-11 col-xs-12 lang-en', 'placeholder' => 'english']) }}
</div>