@extends('admin.body')
@section('title', 'Категория')

@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ URL::to('admin/categories') }}">Категории</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование категории </small> </h1>
    </div>

    @include('admin.partials.errors')

    @if(!isset($data))
    {{ Form::open(['url' => 'admin/categories', 'class' => 'form-horizontal']) }}
    @else
    {{ Form::open(['url' => 'admin/categories/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif
    
    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <!--
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-yellow btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить и закрыть</button>
            </div>-->
            <div class="col-sm-2 ">
                <label>
                	{{ Form::checkbox('top',  1, (isset($data) && $data->top == 1 ? true : false), ['class' => 'ace', 'checked']) }}
                    <span class="lbl showTip L7"> На главную </span>
                </label>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">
                        
                        @if (isset($data))
                        <div class="btn btn-link">
                            <i class="ace-icon fa fa- bigger-120 green"></i>
                            ID: {{ $data->id }}
                        </div>
                        
                        <div class="btn btn-link">
                            <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                             Изменен: {{ $data->updated_at }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name[ru]', 'Заголовок', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group lang-ro">
                {{ Form::label('name[ro]', 'Заголовок рум', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('name[en]', 'Заголовок англ', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[en]', (isset($data->name_en) ? $data->name_en : old('name_en')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group hidden">
                {{ Form::label('rent_days', 'Мин. дни аренды', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('rent_days', (isset($data->rent_days) ? $data->rent_days : old('rent_days')), array('class' => 'col-sm-11 col-xs-12', 'id' => 'rent-days')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group hide">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="date" name="date" id="mydate" class="form-control date-picker"
                               data-date-format="yyyy-mm-dd"
                               value="{{ (isset($data->created_at) ? date('Y-m-d', strtotime($data->created_at)) : old('date', Date::now()->format('Y-m-d'))) }}" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12 showTip L1')) }}
                </div>
            </div>
            
            {{--<div class="form-group showTip L13">
                {{ Form::label('parent', 'Родитель', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                
                <div class="col-sm-9 ">
                    @if(isset($parents))
                    {{ Form::select('parent[]', $categories, $parents, ['multiple'=>'multiple','id'=>'chosencat','class'=>'tag-input-style col-sm-11 control-label no-padding-right']) }}
	                @else
	                {{ Form::select('parent[]', $categories, '', ['multiple'=>'multiple','id'=>'chosencat','class'=>'tag-input-style col-sm-11 control-label no-padding-right']) }}
	                @endif
                </div>
            </div>--}}
            <div class="form-group">
                {{ Form::label('sort', 'Порядок сортировки', ['class'=>'col-sm-3 control-label no-padding-right', 'style' => 'padding-top:0px;']) }}

                <div class="col-sm-9">
                    {{ Form::text('sort', (isset($data->sort) ? $data->sort : old('sort')), array('class' => 'col-sm-11 col-xs-12 showTip L14')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>
    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#ru" data-toggle="tab">Описание</a>
            </li>
            {{--<li>
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>--}}
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="ru">

                <div class="tabbable  tabs-left">

                 <ul id="myTab" class="nav nav-tabs">
                   <li class="active">
                      <a href="#descRu" data-toggle="tab">Описание</a>
                   </li>
                 </ul>

                 <div class="tab-content">
                   <div class="tab-pane in active" id="descRu">
                     {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'ckeditor', 'id' => 'editor')) }}
                   </div>

                 </div>

                </div>
             </div>

            @include('admin.partials.meta')

            {{--@include('admin.partials.photos', ['table' => 'categories', 'table_id' => isset($data->id) ? $data->id : 0])--}}

        </div>

</div>

<div class="form-actions hide">
{{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
</div>

{{ Form::close() }}
@endsection

@section('styles')
{!! HTML::style('ace/assets/css/datepicker.css') !!}
{!! HTML::style('ace/assets/css/chosen.css') !!}

<style>
    div#tipDiv {
        font-size:12px; line-height:1.2; letter-spacing: .2px;
        color:#000; background-color:white; padding: 2px;

        padding:4px;
        width:320px;
        box-shadow: 0 1px 5px 0 rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.12);
    }
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    #sortable {
        border: 1px solid #eee;
        min-height: 20px;
        list-style-type: none;
        margin: 0;
        padding: 5px 0 0 0;
        margin-right: 10px;
        width: 100%;
    }

    #sortable li {
        margin: 0 5px 5px 5px;
        padding: 5px;
        border: 1px solid #ccc;
        background-color: #cce2c1;
    }

</style>
@endsection


@section('scripts')
{!! HTML::script('ace/dist/js/dw_tooltip_c.js') !!}

<script type="text/javascript">

dw_Tooltip.defaultProps = {
}
dw_Tooltip.content_vars = {
    L1: '<b>URL</b> категории должен быть латинскими маленькими символами и вместо пробела должен быть симол нижего подчеркивания <b>"_"</b>',
    L7: 'При выборе данного пункта, категория будет отображена на странице <b>Парк оборудования</b>',
    L8: 'Если данная <b>категория</b> будет отображаеться на <b>главной</b> странице, то загружаемое фото должно быть с разрешением <b>767 x 767</b> <br/><br/>В ином случае загружаемое изображение должно быть с разрешением <b>100 x 100</b>, иметь формат <b>png</b> и быть цветом <b>#b5b5b5</b>',
    L9: 'Изображение для обложки должно быть с разрешением в <b>2560 x 1440</b>',
    L10: '<b>Title</b> это название страницы, которое отображается в самом <b>верхнем поле</b> браузера. Также содержание title отображается в выдаче поисковых систем по запросам пользователей',
    L11: '<b>Meta Description</b> должен содержать <b>краткое описание</b> страницы. Достаточно добавить одно-два небольших предложения, в которых указать о чём и для кого эта страница',
    L12: '<b>Meta Keywords</b> - Те слова, которые наиболее полно характеризуют содержимое страницы и будут для нее ключевыми. Это могут быть как <b>отдельные слова</b>, так и <b>словосочетания</b>, но они <b>обязательно</b> должны встречаться в тексте на странице.',  
    L13: 'Выберите <b>родителя</b> категории к которому будет относиться ваша <b>категория</b>',
    L14: 'Укажите порядок сортировки категории'
}
</script>

@include('admin.partials.ckeditor')

@include('admin.partials.slug',['input_name'=>'name[ru]'])

@include('admin.partials.datepicker')

@include('admin.partials.chosen')

<script>
if($(window).width() < 640){
    $('.tabbable').removeClass('tabs-left');
}
</script>

    {!! HTML::script('ace/assets/js/jquery-ui.js') !!}

<script>
    $(document).ready(function(){

        $('#sortable').sortable();

        if ($('select[name=type]').val() == 1){
            $('.tabbable').removeClass('hide');
        }

        initDelete();
    });

    $('select[name=type]').on('change', function() {
        if ( $('select[name=type]').val() == 1 ){
            $('.tabbable').removeClass('hide');
        }else{
            $('.tabbable').addClass('hide');
        }
    });

    function addItem() {
        var value   = $('select[name=parameter]').val();
        //проверяем, если уже добавили
        if ($('#sortable li input[value="' + value + '"]').length > 0) {
            toastr.error("Уже добавлен");
            return;
        }

        $li         = $('<li class="ui-state-default"></li>');
        $spansort   = $('<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>');
        $input      = $('<input type="hidden" name="parameters_products[]" value="0" />');
        $input2     = $('<input type="hidden" name="parameters[]" value="' + value + '" />');
        $spantext   = $('select[name=parameter] option:selected').text() + '  ';
        $delete     = $('<i class="ace-icon fa fa-times-circle delete"></i>');

        $li.append($spansort).append($input).append($input2).append($spantext).append($delete);
        $('#sortable').append($li);

        initDelete();
    }

    function initDelete() {
        $('#sortable li i.delete').unbind( "click" ).click(function() {

            toastr.error("Требует доработки в базе. Нужна проверка если нигде не задействован данный параметр");

            return false;

            var id = $(this).closest('li').find('input[name="parameters_products[]"]').val();
            var parameters_id = $(this).closest('li').find('input[name="parameters[]"]').val();

            $this = $(this);

            $.get("{{ URL::to('admin/json/remove-product-parameter') }}",
                    {
                        'id': id,
                        'parameters_id': parameters_id
                    },
                    function(response) {
                        if (response.success == "false") {
                            toastr.error(response.data);
                            return false;
                        }
                        if (response.success == "true") {
                            $this.closest('li').remove();
                            toastr.success("Удалено");
                            return true;
                        }
                        toastr.error(response);
                    }
            );
        });
    }
</script>
{!! HTML::script('ace/assets/js/fuelux/fuelux.spinner.js') !!}
{!! HTML::script('ace/assets/js/ace/elements.spinner.js') !!}

<script>
    $(function () {
        $('#rent-days').ace_spinner({
            value: "@if(isset($data->rent_days)){{$data->rent_days}}@else 1 @endif",
            min: 1,
            max: 99,
            step: 1,
            on_sides: true,
            icon_up: 'ace-icon fa fa-plus bigger-110',
            icon_down: 'ace-icon fa fa-minus bigger-110',
            btn_up_class: 'btn-success',
            btn_down_class: 'btn-danger'
        });
    });
</script>

@endsection
