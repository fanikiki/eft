@extends('admin.common.list',
    [
        'title'       =>  'Производители',
        'desc'        =>  'Список производителей',
        'model'       =>  'manufacturers',
        'fields'      =>  ['name' => 'Наименование', 'slug' => 'Ссылка'],
        'data'        =>  $data
    ]
)
