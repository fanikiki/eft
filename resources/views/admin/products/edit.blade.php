@if(!isset($data))
    {{ Form::open(['url' => 'admin/products', 'class' => 'form-horizontal', 'id' => 'products']) }}
@else
    {{ Form::open(['url' => 'admin/products/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'products']) }}
@endif

{{ Form::hidden('category_id', app('request')->input('category_id')) }}
{{ Form::hidden('model_id', (isset($data->model_id) ? $data->model_id : app('request')->input('model_id'))) }}

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('product_number', 'Артикул', ['class'=>'col-sm-6 control-label no-padding-right']) }}
            <div class="col-sm-6">
                {{ Form::text('product_number', (isset($data->product_number) ? $data->product_number : old('product_number')), array('class' => 'col-sm-11 col-xs-12')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('price', 'Цена', ['class'=>'col-sm-6 control-label no-padding-right']) }}
            <div class="col-sm-6">
                {{ Form::text('price', (isset($data->price) ? $data->price : old('price')), array('class' => 'col-sm-11 col-xs-12')) }}
            </div>
        </div>
        {{--<div class="form-group">
            {{ Form::label('sale_price', 'Цена со скидкой', ['class'=>'col-sm-6 control-label no-padding-right']) }}
            <div class="col-sm-6">
                {{ Form::text('sale_price', (isset($data->sale_price) ? $data->sale_price : old('sale_price')), array('class' => 'col-sm-11 col-xs-12')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('quantity', 'Количество', ['class'=>'col-sm-6 control-label no-padding-right']) }}
            <div class="col-sm-6">
                {{ Form::text('quantity', (isset($data->quantity) ? $data->quantity : old('quantity')), array('class' => 'col-sm-11 col-xs-12')) }}
            </div>
        </div>--}}
    </div><!-- /.col-sm-6 -->
    {{--<div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('radius', 'Радиус', ['class'=>'col-sm-6 control-label no-padding-right']) }}
            <div class="col-sm-6">
                {{ Form::text('radius', (isset($data->radius) ? $data->radius : old('radius')), array('class' => 'col-sm-11 col-xs-12')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('width', 'Ширина', ['class'=>'col-sm-6 control-label no-padding-right']) }}
            <div class="col-sm-6">
                {{ Form::text('width', (isset($data->width) ? $data->width : old('width')), array('class' => 'col-sm-11 col-xs-12')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('height', 'Высота', ['class'=>'col-sm-6 control-label no-padding-right']) }}
            <div class="col-sm-6">
                {{ Form::text('height', (isset($data->height) ? $data->height : old('height')), array('class' => 'col-sm-11 col-xs-12')) }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-offset-6">
                {{ Form::checkbox('top',  1, (isset($data) && $data->top == 1 ? true : false), ['class' => 'ace']) }}
                <span class="lbl"> Топ</span>
            </label>
        </div>
    </div>--}}<!-- /.col-sm-6 -->
</div><!-- /.row -->

<div class="space"></div>

<div class="tabbable">
    <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
        {{--<li class="active">
            <a href="#productsParameters" data-toggle="tab">Параметры</a>
        </li>--}}
        <li>
            <a href="#productsPhotos" data-toggle="tab">Фото</a>
        </li>
        {{--@if (isset($data->model))
        <li>
            <a href="#productsPhotosDB" data-toggle="tab">Фото из базы</a>
        </li>
        @endif--}}
        <li class="active">
            <a href="#productsDesc" data-toggle="tab">Описание</a>
        </li>
        {{--<li>
            <a href="#productsmeta" data-toggle="tab">META</a>
        </li>--}}
    </ul>

    <div class="tab-content">
        {{--<div class="tab-pane active" id="productsParameters">
            <ul id="products_sortable">
                @if (isset($parameters))
                    @foreach($parameters as $param)
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> {{ $param->parameter->name }} </label>
                                    <div class="col-sm-9">
                                        {{ Form::hidden('parameter_products_id[]', ($param->table == 'products' ? $param->id : 0)) }}
                                        {{ Form::hidden('parameters_id[]', $param->parameter->id) }}
                                        @if($param->parameter->type == 0)
                                                {{ Form::text('values[ru]['.$param->parameter->id.']', $param->value, ['placeholder' => 'Значение', 'class' => 'col-xs-4 input-sm']) }}
                                                {{ Form::text('values[ro]['.$param->parameter->id.']', $param->value_ro, ['placeholder' => 'Значение RO', 'class' => 'col-xs-4 input-sm lang-ro']) }}
                                                {{ Form::text('values[en]['.$param->parameter->id.']', $param->value_en, ['placeholder' => 'Значение EN', 'class' => 'col-xs-4 input-sm lang-en']) }}
                                        @elseif($param->parameter->type == 1)
                                                {{ Form::select('values_id['.$param->parameter->id.']',
                                                    [0 => 'не выбрано'] + $param->parameter->values->pluck('value', 'id')->toArray(),
                                                    $param->values_id,
                                                    ['class' => 'col-xs-12 col-sm-11'])
                                                }}

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </ul>
        </div>--}}

        <div class="tab-pane active" id="productsDesc">
            <div class="tabbable tabs-left">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#productsDescRu" data-toggle="tab">Описание</a>
                    </li>
                    <li class="lang-ro">
                        <a href="#productsDescRo" data-toggle="tab">Описание на румынском</a>
                    </li>
                    <li class="lang-en">
                        <a href="#productsDescEn" data-toggle="tab">Описание на английском</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="productsDescRu">
                        {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'ckeditor', 'id' => uniqid('id'))) }}
                    </div>
                    <div class="tab-pane" id="productsDescRo">
                        {{ Form::textarea('description[ro]', (isset($data->description_ro) ? $data->description_ro : old('description_ro')), array('class' => 'ckeditor', 'id' => uniqid('id'))) }}
                    </div>
                    <div class="tab-pane" id="productsDescEn">
                        {{ Form::textarea('description[en]', (isset($data->description_en) ? $data->description_en : old('description_en')), array('class' => 'ckeditor', 'id' => uniqid('id'))) }}
                    </div>
                </div>
            </div>
        </div>

        @if (isset($data->model))
        <div class="tab-pane" id="productsPhotosDB">
            <div class="row">
                <div class="col-xs-12 center">
                    @if ($data->model->category_id == 1)
                        <img src="/uploaded/tyres_images/{{ $data->getSmallPhoto() }}" height="300">
                    @elseif ($data->model->category_id == 2)
                        <img src="/uploaded/disks_images/{{ $data->getSmallPhoto() }}" height="300">
                    @endif
                </div>
            </div>
        </div>
        @endif


        {{--@include('admin.partials.meta', ['form_id' => 'products'])--}}

        @include('admin.partials.photos', ['table' => 'products', 'div_id' => 'productsPhotos', 'table_id' => isset($data->id) ? $data->id : 0])
    </div>
</div>

@include('admin.partials.ckeditor', ['form_id' => 'products'])

{!! HTML::script('ace/assets/js/jquery-ui.js') !!}

<script>
    $(document).ready(function(){

        //$('#products_sortable').sortable();

        //getCategoryParameters();
    });

    function getCategoryParameters(){
        var category_id = $('input[name=category_id]').val();


        $.get("{{ URL::to('admin/json/get-category-parameters') }}",
                {
                    'category_id': category_id
                },
                function(response) {
                    if (response.success == "true") {
                        $.each(response.data, function() {
                            addItem(this);
                        });
                        toastr.success("Добавлены параметры категории");
                        return true;
                    }
                    toastr.error(response);
                }
        );
    }

    function addItem(item) {
        $li         = $('<li class="ui-state-default"></li>');
        $input_hidd = $('<input type="hidden" name="parameters_id[]" value="' + item.id + '" />');
        $labeltext   = $('<label class="param-label">' + item.name + '  ' + '</label>');

        if (item.type == 0) {
            $input = $('<input type="text" name="values[]" />');
        }

        if (item.type == 1) {
            $input = $('<select name="values[]"></select>');
            $.each(item, function(key, value) {
                $option = $('<option name="' + key + '">' + value + '</option>');
                $input.append($option);
            });
        }

        $li.append($input_hidd).append($labeltext).append($input);
        $('#products_sortable').append($li);
    }
</script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    #products_sortable {
        border: 1px solid #eee;
        min-height: 20px;
        list-style-type: none;
        margin: 0;
        padding: 5px 0 0 0;
        margin-right: 10px;
        width: 100%;
    }

    #products_sortable li {
        margin: 0 5px 5px 5px;
        padding: 5px;
        border: 1px solid #ccc;
        background-color: #cce2c1;
    }
</style>