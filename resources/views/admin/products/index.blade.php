@extends('admin.body')
@section('title', 'Номенклатура')
@section('centerbox')
    <div class="page-header">
        <h1> Товары <small><i class="ace-icon fa fa-angle-double-right"></i> {{ $category->name or 'Все товары' }} </small> </h1>
    </div>

    @include('admin.partials.messages')

    <div class="row">
        <div class="col-xs-12">
			<div class="row">
				<div class="col-sm-3">
					<a class="btn btn-success" href="{{ URL::to('admin/models/create') }}?category_id={{ $category->id or 0 }}">
		                <i class="ace-icon fa fa-plus-square-o  bigger-120"></i>
		                Создать товар
		            </a>
				</div>
				<div class="col-sm-9">

					<div class="col-xs-12">
						{{ Form::open(['url' => 'admin/products', 'method' => 'get', 'class' => 'form-horizontal', 'id' => 'products']) }}
						{{ Form::hidden('category_id', $category->id) }}
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label class="col-sm-2 hidden-xs control-label">Фильтры:</label>
									<div class="col-sm-10 col-xs-12">
										{{ Form::select('manufacturer', [0 => 'Производитель'] + $needs['manufacturers'], $filters['manufacturer'],
                                                    ['class' => 'chosen-select tag-input-style'])
                                        }}

										{{ Form::text('model', $filters['model'], ['placeholder' => 'Модель']) }}

										{{--{{ Form::select('r', $needs['radius_arr'], $filters['radius']) }}

										{{ Form::select('w', $needs['width_arr'], $filters['width']) }}

										{{ Form::select('h', $needs['height_arr'], $filters['height']) }}--}}

										<button class="btn btn-sm btn-primary" type="submit">
											<i class="ace-icon fa fa-filter bigger-120"></i>
											Применить
										</button>
										<a class="btn btn-sm" href="{{ url('admin/products?category_id=' . $category->id) }}">
											<i class="ace-icon fa fa-filter bigger-120"></i>
											Сбросить
										</a>
									</div>
								</div><!-- /.form-group -->

							</div>
						</div>
						{{ Form::close() }}
					</div>
				</div>
			</div>

            <div class="clearfix"><div class="pull-right tableTools-container"></div></div>
            <div class="table-header"> Список товаров </div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th align="center">№</th>
                        <th align="center">Производитель</th>
                        <th align="center">Модель</th>
                        <th align="center">Комплект</th>
                    </tr>
                </thead>
                <tbody bgcolor="white">


                @foreach($data as $key => $model)
                    @forelse($model->products as $product)
                        @include('admin.products.partials.row' , ['counter' => $loop->iteration])
                    @empty
                        <? $product = null; ?>
                        @include('admin.products.partials.row' , ['counter' => $loop->iteration])
                    @endforelse
                @endforeach
                </tbody>
            </table>

            {{ $data->appends([
            	'category_id' => $category->id,
            	'manufacturer' => $filters['manufacturer'],
            	'model' => $filters['model']
            	/*'radius' => $filters['radius'],
            	'width' => $filters['width'],
            	'height' => $filters['height'],*/
            	])->links() }}

        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@endsection

@section('styles')
    {{HTML::style('ace/assets/css/dataTables.responsive.css')}}
    {{HTML::style('ace/assets/css/chosen.css')}}
@endsection

@section('scripts')

    @include('admin.common.modals')

    @include('admin.partials.ckeditor')

	{!! HTML::script('ace/assets/js/chosen.jquery.js') !!}
	<script>
		 $('.chosen-select').chosen({width: '200px'});
	</script>

@append