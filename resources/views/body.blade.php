<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="ru">

<head>
    <base href="{{route('index')}}/">
    <link rel="canonical" href="{{Request::url()}}"/>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', trans('common.meta_title'))</title>
    <meta name="keywords" content="@yield('meta_keywords', trans('common.meta_keywords'))"/>
    <meta name="description" content="@yield('meta_description', trans('common.meta_description'))"/>
    <!-- Favicon -->
    <link rel="icon" href="favicon.ico?" type="image/x-icon">

    <meta property="og:type" content="website"/>
    <meta property="og:locale" content="ru_RU"/>
    <meta property="og:title" content="@yield('title', trans('common.meta_title'))"/>
    <meta property="og:description" content="@yield('meta_description', trans('common.meta_description'))"/>
    <meta property="og:url" content="{{route('index')}}"/>
    <meta property="og:image" content="{{ route('index') }}/images/eftpluslogo.png"/>
    <meta property="og:site_name" content="EFT-PLUS"/>

    <!-- CSS Global -->
    <link href="js/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="js/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="js/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/plugins/prettyphoto/css/prettyPhoto.css" rel="stylesheet">
    <link href="js/plugins/owl-carousel2/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="js/plugins/owl-carousel2/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="js/plugins/animate/animate.min.css" rel="stylesheet">
    <link href="js/plugins/swiper/css/swiper.min.css" rel="stylesheet">
    <link href="js/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
{{--<link rel="stylesheet" href="css/vendor.css">--}}

<!-- Theme CSS -->
    <link href="css/theme-orange-1.css" rel="stylesheet">

    <!-- Custom CSS-->
    <link href="css/xsort.css?v={{ filemtime(public_path('css/xsort.css')) }}" rel="stylesheet">

    <!-- Funky Radio -->
    <link href="css/product-package.css" rel="stylesheet">

    <!-- Head Libs -->
    <script src="js/plugins/modernizr.custom.js"></script>

    <!--[if lt IE 9]>
    <script src="js/plugins/iesupport/html5shiv.js"></script>
    <script src="js/plugins/iesupport/respond.min.js"></script>
    <![endif]-->
    @yield('styles')

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();
   for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
   k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(49647466, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49647466" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-250168558-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-250168558-1');
</script>
<!-- /Google tag (gtag.js) -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5JQB2HC');</script>
<!-- End Google Tag Manager -->

</head>

<body id="home" class="wide">
    
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JQB2HC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- PACKAGE MODAL -->
<div class="modal fade" tabindex="-1" role="dialog" id="my-modal-order">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-title orange">Заявка на аренду</div>
            </div>
            <form id="frm_calc" method="post" action="send-order-calc">
                <div class="modal-body">
                    <div class="form-group has-label has-icon">
                        <label for="fio">ФИО</label>
                        <input type="text" class="form-control" id="fio" name="fio" placeholder="ФИО">
                        <span class="form-control-icon"><i class="fa fa-user"></i></span>
                    </div>
                    <div class="form-group has-label has-icon">
                        <label for="mobile">Телефон</label>
                        <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Телефон">
                        <span class="form-control-icon"><i class="fa fa-phone"></i></span>
                    </div>
                    <div class="form-group has-label has-icon">
                        <label for="email">E-mail</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="E-mail">
                        <span class="form-control-icon"><i class="fa fa-envelope"></i></span>
                    </div>
                    @if(Route::currentRouteName() == 'index')
                        <div class="form-group">
                            <input type="hidden" name="g-recaptcha-response" class="g-recaptcha">
                        </div>
                    @endif
                    <p class="font-big orange">
                        Цена: <span id="total"></span> руб.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-submit btn-theme" id="calc-order">Заказать</button>
                </div>
                {{ csrf_field() }}
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- PACKAGE MODAL -->

<!-- SUCCESS SUBMISSION -->
<div class="modal fade" tabindex="-1" role="dialog" id="order_success">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-title orange">Заказ принят</div>
            </div>
            <div class="modal-body">
                <p><b>Добрый день!</b></p>
                <p>Благодарим за заказ через наш сайт. Мы обязательно свяжемся с Вами в ближайшее время!</p>
                <p>Вы можете связаться с нами по телефону:
                    <br>
                    <span style="font-size: 16px; color: #ff6600;">+7 (495) 212-1717</span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-submit" id="calc-order" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- SUCCESS SUBMISSION -->

<!-- PRELOADER -->
{{--<div id="preloader">
    <div id="preloader-status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
        <div id="preloader-title">Загрузка</div>
    </div>
</div>--}}
<!-- /PRELOADER -->

<div class="main-wrap">
    @include('partials.header')
    <div class="container">
        @include('partials.aside')
        <section class="col-xs-12 col-sm-10 right-bar center-content clearfix">
            @yield('centerbox')
        </section>
    </div>
    @include('partials.contact-form')
    @include('partials.footer')
</div>


{{--<div class="wrapper">
    @include('partials.header')
    <div class="content-area">
        @yield('centerbox')
    </div>
    @include('partials.footer')
</div>--}}

<script src="js/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="js/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="js/plugins/validate/jquery.validate.min.js"></script>
<script src="js/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<script src="js/plugins/superfish/js/superfish.min.js"></script>
<script src="js/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script>
<script src="js/plugins/owl-carousel2/owl.carousel.min.js"></script>
<script src="js/plugins/jquery.sticky.min.js"></script>
<script src="js/plugins/jquery.easing.min.js"></script>
<script src="js/plugins/jquery.smoothscroll.min.js"></script>
<script src="js/plugins/smooth-scrollbar.min.js"></script>
<script src="js/plugins/swiper/js/swiper.jquery.min.js"></script>
<script src="js/plugins/datetimepicker/js/moment-with-locales.min.js"></script>
<script src="js/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
{{--<script src="js/vendor.js"></script>--}}

<!-- JS Page Level -->
<script src="js/theme-ajax-mail.js"></script>
<script src="js/theme.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&key=AIzaSyCTAt5dBDvupMd8vOD2A_frx5DVEfpWtcM&language=ru"></script>-->
<!--<script src="assets/js/vue.min.js"></script>
<script src="assets/js/eft-calculator.js"></script>-->
<script src="js/main.js?v={{filemtime(public_path('js/main.js'))}}"></script>
<script src="js/cities.js?20180213"></script>
<script src="vendor/laravel-filemanager/js/jquery.form.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js?hl=ru&render={{ config('services.recaptcha.sitekey') }}'></script>
<script>
    grecaptcha.ready(function () {
        grecaptcha.execute("{{ config('services.recaptcha.sitekey') }}", {action: 'contact'}).then(function (token) {
            let recaptchaResponse = $('.g-recaptcha');
            recaptchaResponse.val(token);
        });
    });
</script>
<script src="js/ofi.min.js"></script>

<script src="js/plugins/sweetalert2/sweetalert2.js"></script>
<link rel="stylesheet" type="text/css" href="js/plugins/sweetalert2/sweetalert2.css">
<script src="js/plugins/validate/jquery.validate.min.js"></script>

@yield('scripts')

{{--<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "url": "{{ route('index') }}",
  "logo": "{{ route('index') }}/images/arenda2.png",
  "name": "EFT-ARENDA",
  "image": "{{ route('index') }}/images/arenda2.png",
  "description": "Мы специализируемся на аренде геодезического оборудования, что позволяет нам предлагать пользователям лучшие условия.",
  "telephone": "8 (495) 390 20 20",
  "email": "mailto:info@eft.by",
  "address" : {
    "@type": "PostalAddress",
    "streetAddress": "ул. Новодмитровская д.2, к.2, 9 этаж",
    "addressLocality": "г. Москва",
    "addressRegion": "Москва",
    "postalCode": "postalCode",
    "addressCountry" : {
      "@type": "Country",
      "name": "Russia"
    }
  },
  "openingHours": "Mo,Tu,We,Th,Fr 09:00-17:00",
  "hasMap": "https://www.google.com/maps/place/Sushi+Imari/@33.664141,-117.879423,17z/data=!3m1!4b1!4m2!3m1!1s0x80dcdfaa9f9de2a5:0x48ad2abe6bb60a3b?hl=en",
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": "40.75",
    "longitude": "73.98"
  },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.4",
    "reviewCount": "20"
  }
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "EFT-ARENDA",
  "alternateName": "EFT-ARENDA",
  "url": "{{route('index')}}",
  "logo": "{{route('index')}}/images/arenda2.png",
  "contactPoint": [{
    "@type": "ContactPoint",
    "telephone": "+7 (495) 212-17-17",
    "contactType": "Customer Service"
  }],
  "sameAs": [
    "https://www.facebook.com/EFTGroup/",
    "http://www.youtube.com/c/EFTGroup-channel",
    "https://www.instagram.com/eftgroup.ru"
  ]
}
</script>--}}

</body>

</html>




