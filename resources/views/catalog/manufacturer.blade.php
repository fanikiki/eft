@extends('body') 
@section('title', 'Каталог ' . $manufacturer->name)
@section('centerbox') 
<section class="content">
  <!-- begin Breadcrumbs -->
  <?php if ($category->id == 1) { $items = ['Каталог шин' => route('tyres')]; } else { $items = ['Каталог дисков' => route('diski')]; } ?> @include('partials.breadcrumbs', ['title' => 'Каталог ' . $manufacturer->name, 'items' => $items + ['Каталог ' . $manufacturer->name
  => '']])
  <!-- end Breadcrumbs -->
  <div class="container">
    <div class="index-content">
      <div class="col-md-9 col-xs-12">
        <div class="row catalog-tyre-wrapper" layout="row" layout-align-xs="center stretch" layout-wrap="" layout-align="start stretch">
          @foreach ($models as $model)
          <div class="col-md-4 col-sm-4 col-xs-12" layout-xs="column" layout-align-xs="start center">
            <div layout="column" layout-align="space-between stretch" class="tyre-container">
              <div class="tyre-type" layout="column">
                @if (in_array($model->products{0}->getSeason(), [1, 2]))
                <div class="season-winter"></div>
                @endif @if (in_array($model->products{0}->getSeason(), [1, 3]))
                <div class="season-summer"></div>
                @endif
              </div>
              <a href="{{ route('get-model', [$category->slug, $manufacturer->slug, $model->slug]) }}" class="tyre-logo">

                                <img src="{{ $model->products{0}->getSmallPhotoPath() }}" alt="{{ $model->getFullName() }}">

                            </a>  <a href="{{ route('get-model', [$category->slug, $manufacturer->slug, $model->slug]) }}" class="tyre-title">{{ $model->getFullName() }}</a>
              <div class="tyre-price">
                <span>Цена от {{ $model->products{0}->price }} руб.</span>
              </div>
            </div>
          </div>
          @endforeach
        </div>

        {{ $models->links() }}

        <div class="other-brand" layout="column">
          <h4 class="other-title">Другие производители</h4>
          <div class="other-links" layout="row" layout-wrap="">
            @foreach ($manufacturers_without_photo as $item)
            <a href="{{ route('get-manufacturer', [$category->slug, $item->slug]) }}" title="{{ $item->name }}">{{ $item->name }}</a>
@endforeach
          </div>
        </div>
        <p class="tyre-description">Каталог шин MICHELIN (michelin) в Москве - зимняя, летняя, всесезонная резина. Самый большой каталог шин в Москве представленный на этой странице предлагает Вашему вниманию интернет-магазин 1tyres.ru. Доступный и логически построенный интерфейс
          каталога, позволяет легко подобрать искомую модель шины, если Вам известна ее Марка, модель и типоразмер. Первым шагом, который необходимо сделать, зайдя в каталог резины, является выбор транспортного средства. Страница каталога предлагает шины
          для легковых и коммерческих авто, мотоциклов и грузовых автомобилей. Определившись с типом автомобиля, выбираем марку. Как правило, производители шин, чей ассортимент представлен на страницах нашего интернет-магазина, предлагают широкий ассортимент
          моделей для всех типов транспортных средств. В случае если Вы не нашли подходящей марки резины, стоит попробовать повторить поиск немного позже, так как ассортимент на сайте меняется в режиме реального времени.</p>
      </div>
      <!-- begin Sidebar -->
      @include('partials.sidebar')
      <!-- end Sidebar -->
    </div>
  </div>
</section>
@stop