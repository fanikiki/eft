<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
Заявка на демо-показ для <b>{{ $product }}</b>
<br/>
<br/>
<br/>Контактные данные заказчика:
<br/>
<b>ФИО:</b> {{ $fio }}
<br/>
<b>Название компании:</b> {{ $company }}
<br/>
<b>Телефон:</b> {{ $phone }}
<br/>
<b>Комментарий:</b> {{ $comment }}
<br/>