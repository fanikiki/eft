<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<p>Новая заявка на аренду:</p>
<p>------</p>
<b>Тип оборудования:</b> {{ $calc_type }}
<br>
<b>Производитель:</b> {{ $calc_manufacturer }}
<br>
<b>Модель:</b> {{ $calc_model }}
<br>
<b>Комплект:</b> {{ $calc_package }}
<br>
<b>Количество:</b> {{ $quantity }}
<br>
<b>Дата с:</b> {{ $date_from }}
<br>
<b>Дата по:</b> {{ $date_to }}
<p>------</p>
<b>ФИО:</b> {{ $fio }}
<br>
<b>Телефон:</b> {{ $mobile }}
<br>
<b>Email:</b> {{ $email }}
<br>