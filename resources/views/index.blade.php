@extends('body')
@section('centerbox')

    <div class="row page-wrapper">
        <div class="row">
            <div class="col-xs-12 no-padding">
                @include('partials.jssor-slider')
            </div>
        </div>
        <div class="row test2">
            <div class="col-sm-12 index-products-row">
                @if($products->count() > 0)
                    @foreach ($products as $product)
                        <div class="col-sm-6 col-md-4 index-product-col">
                            <div class="thumbnail no-border thumbnail-car-card">
                                <div class="media">
                                    <a class="media-link"
                                       href="{{route('get-model', [$product->categories->slug, $product->slug])}}">
                                        @if(isset($product->photos{0}))
                                            <img src="uploaded/{{$product->photos{0}->source}}"
                                                 alt="{{$product->name or ''}}<"/>
                                        @endif
                                    </a>
                                </div>
                                <div class="caption text-center">
                                    <span class="caption-title"><a
                                                href="{{route('get-model', [$product->categories->slug, $product->slug])}}">{{$product->name or ''}}</a></span>
                                    <div class="caption-text">{{$product->price}} руб.</div>
                                    <div class="buttons">
                                        <a class="btn btn-theme ripple-effect"
                                           href="{{route('get-model', [$product->categories->slug, $product->slug])}}">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="js/plugins/jssor/js/jssor.slider.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jssor_1_slider_init = function() {

                var transition = [
                    { $Duration: 1200, x: 0.3, y: -0.3, $Delay: 20, $Cols: 10, $Rows: 5, $Opacity: 2, $Clip: 15, $During: { $Left: [0.2, 0.8], $Top: [0.2, 0.8] }, $SlideOut: true, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 260, $Easing: { $Left: $Jease$.$InJump, $Top: $Jease$.$InJump, $Clip: $Jease$.$Swing }, $Round: { $Left: 0.8, $Top: 0.8 } }
            ];

                var jssor_1_options = {
                    $AutoPlay: 1,
                    $Idle: 2500,
                    $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                        $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                        $Transitions: transition,            //[Required] An array of slideshow transitions to play slideshow
                        $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                        $ShowLink: true                                 //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                    },
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$
                    },
                    $BulletNavigatorOptions: {
                        $Class: $JssorBulletNavigator$
                    }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                /*responsive*/

                var MAX_WIDTH = 900;

                function ScaleSlider() {
                    var containerElement = jssor_1_slider.$Elmt.parentNode;
                    var containerWidth = containerElement.clientWidth;

                    if (containerWidth) {

                        var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                        jssor_1_slider.$ScaleWidth(expectedWidth);
                    }
                    else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }

                ScaleSlider();

                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);

                /*responsive*/
            };

            jssor_1_slider_init();
        });
    </script>
@endsection