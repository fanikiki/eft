@extends('body')
@section('title', 'Новости')
@section('centerbox')
    <ol class="breadcrumb">
        <li><a href="{{route('index')}}">Главная</a></li>
        <li class="active">Новости</li>
    </ol>
    <div class="row">
        <div class="col-sm-12">
                <div class="page-header orange-center no-margin">
                    <h2 class="no-margin">Новости</h2>
                </div>
                @foreach($data as $news)
                    <div class="media">
                        <div class="media-left">
                            @if(isset($news->photos{0}))
                                <a href="{{ route('get-news', $news->slug) }}"><img src="uploaded/{{$news->photos{0}->source}}" class="media-object"></a>
                            @endif
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="{{ route('get-news', $news->slug) }}"><b>{{$news->name}}</b></a></h4>
                            {!! str_limit(strip_tags($news->description), 300) !!}
                        </div>
                        <a href="{{ route('get-news', $news->slug) }}" class="btn btn-sm pull-right btn-news-more">Подробнее</a>
                    </div>
                @endforeach
        </div>
    </div>
@endsection