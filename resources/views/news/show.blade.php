@extends('body')
@section('title', $news->name)
@section('centerbox')
    <ol class="breadcrumb">
        <li><a href="{{route('index')}}">Главная</a></li>
        <li><a href="{{route('news')}}">Новости</a></li>
        <li class="active">{{$news->name}}</li>
    </ol>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header orange-center">
                <h2>{{$news->name}}</h2>
            </div>
            @if(isset($news->photos{0}))
            <div class="news-photo">
                <img src="uploaded/{{$news->photos{0}->source}}" class="img-responsive">
            </div>
            @endif
            <div class="news-description">
                {!! $news->description !!}
            </div>
        </div>
    </div>
@endsection