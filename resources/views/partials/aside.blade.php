<aside class="col-sm-2 left-bar hidden-xs" itemscope itemtype="http://schema.org/ItemList">
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <meta itemprop="itemListOrder" content="http://schema.org/ItemListOrderDescending" />
        @if($categories->count() > 0)
        @foreach($categories as $category)
        @if(!in_array($category->id, [1]))
        <li><a href="{{ route('get-category', $category->slug) }}" itemprop="url">{{$category->name}}</a></li>
        @endif
        @endforeach
        @endif
        @if($content->count() > 0)
        <div class="content-list">
            <li><a href="{{ route('get-category', 'arenda') }}" itemprop="url">Аренда</a></li>
            @foreach($content as $c)
            @if(!in_array($c->id, [2, 8, 9, 10]))
            <li><a href="{{ route('content', $c->slug) }}" itemprop="url">{{$c->name}}</a></li>
            @endif
            @endforeach
        </div>
        @endif
        <div class="content-list">
            <li><a href="{{ route('content', $tradein->slug) }}" itemprop="url">{{$tradein->name}}</a></li>
            <li><a href="{{ route('test-drive') }}" itemprop="url">Test-drive</a></li>
            <li><a href="{{ route('demo') }}" itemprop="url">Демо-показ</a></li>
        </div>
    </ul>
</aside>