<!-- begin breadcrumbs -->
<div class="breadcrumbs">
  <div class="container">
    <div class="container-fluid">
      <h2 class="breadcrumbs-title">{{ $title }}</h2>
      <ol vocab="http://schema.org/" typeof="BreadcrumbList">
        <li>
          <a href="{{ route('index') }}">Главная</a>
        </li>
        @foreach ($items as $name => $url) @if ($url == '')
        <li>{{ $name }}</li>
        @else
        <li><a href="{{ $url }}">{{ $name }}</a>
        </li>
        @endif @endforeach
      </ol>
    </div>
  </div>
</div>


<script type="application/ld+json">
{
 "@context": "http://schema.org",
 "@type": "BreadcrumbList",
 "itemListElement":
 [
  {
   "@type": "ListItem",
   "position": 1,
   "item":
   {
    "@id": "{{ route('index') }}",
    "name": "Главная"
    }
  },

  @foreach ($items as $name=>$url )
  {
  "@type": "ListItem",
  "position": {{ $loop->iteration + 1 }},
  "item":
   {
     "@id": "@if ($url != '') {{$url}} @else javascript:void(0) @endif",
     "name": "{{$name}}"
   }
  }@if($loop -> last) @else,@endif
  @endforeach
 ]
}
</script>
<!-- end breadcrumbs -->
