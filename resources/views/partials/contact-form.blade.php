<section class="page-section contact-form-section">
    <div class="container full-width">
        <div class="row">
            <div class="section-title newsletter">
                <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-1 col-md-4 col-md-push-1 no-margin-top">
                    <div class="newsletter-container-left">
                        <h3>Подписаться на <span id="newsletter-title">рассылку</span></h3>
                        <p>Мы будем информировать вас о всех новинках по электронной почте</p>
                    </div>
                </div>
                <div class="col-xs-10 col-xs-offset-1 col-sm-5 col-sm-offset-1 col-md-6 col-md-pull-1">
                    <div class="newsletter-container-right">
                        <form method="post" action="{{ route('subscribe') }}" id="subscribe-frm">
                        <div class="input-group input-full-width">
                            <input type="text" name="email" class="form-control input-lg" id="email" placeholder="Email">
                            <span class="input-group-btn">
                                <button class="btn btn-theme btn-subscribe" type="submit">Подписаться</button>
                            </span>
                        </div>
                            <input type="hidden" name="message" placeholder="Enter your message">
                            <input type="hidden" name="g-recaptcha-response" class="g-recaptcha">
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-1 col-md-4 col-md-push-1">
                @if (!empty($contacts_list))
                    <div class="contact-info">
                        <div class="single-info">
                            <i class="fa fa-map-marker"></i>
                            <h3>Адрес:</h3>
                            <p id="contact-address">{{$contacts_list->description_short}}</p>
                        </div>
                        <div class="single-info">
                            <i class="fa fa-phone"></i>
                            <h3>Телефон/Факс:</h3>
                            <p>
                                <a href="tel:{{$contacts_list->description_short_ro}}">{{$contacts_list->description_short_ro}}</a>
                            </p>
                        </div>
                        <div class="single-info">
                            <i class="fa fa-envelope"></i>
                            <h3>Напишите нам:</h3>
                            <p><a href="mailto:info@eft.by">info@eft.by</a></p>
                        </div>
                        <div class="single-info">
                            <i class="fa fa-clock-o"></i>
                            <h3>Время работы:</h3>
                            <p>По будням с 09:30 до 18:00</p>
                        </div>
                    </div>
                @endif

            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-5 col-sm-offset-1 col-md-6 col-md-pull-1">
                <form action="{{route('send-contactform')}}" class="contact-form" method="post" id="contact-form">
                    <div class="form-group">
                        <input type="text" placeholder="ФИО" id="fio" name="fio" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="email" placeholder="Email" id="email" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <textarea cols="30" rows="10" placeholder="Сообщение" name="msg" id="msg" class="form-control"></textarea>
                    </div>
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-theme ripple-effect btn-contact">Отправить</button>

                    <input type="hidden" name="g-recaptcha-response" class="g-recaptcha">
                </form>
            </div>
        </div>
    </div>
</section>