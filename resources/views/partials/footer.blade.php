<footer class="footer">
    <div class="container big-footer">
        <p class="col-lg-3 col-md-3 col-sm- cpy">© {{date('Y')}} ЕФТ ПЛЮС</p>
        <div class="col-lg-4 col-md-5 col-sm-6 col-lg-offset-1 col-md-offsset-2 footer-menu">
            <nav>
                <a href="{{route('index')}}" id="main">Главная</a>
                <a href="{{ route('content', 'about') }}">О компании</a>
                <a href="{{ route('news') }}">Новости</a>
                <a href="{{ route('contacts') }}">Контакты</a>
            </nav>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-lg-offset-1 col-md-offset-1 logos">
            <a href="http://www.youtube.com/c/EFTGroup-channel" target="_blank" rel="nofollow">
                <i class="fa fa-youtube-play fa-lg" aria-hidden="true" style="color: white;"></i>
            </a>
            <a href="https://www.facebook.com/EFTGroup/" target="_blank" rel="nofollow">
                <img src="images/fa-facebook-official.png" class="facebook-logo">
            </a>
            <a href="https://vk.com/eftgroup" target="_blank" rel="nofollow">
                <i class="fa fa-vk fa-lg" aria-hidden="true" style="color: white;"></i>
            </a>
            <a href="https://www.instagram.com/eftgroup.ru" target="_blank" rel="nofollow">
                <i class="fa fa-instagram fa-lg" aria-hidden="true" style="color: white;"></i>
            </a>
        </div>
    </div>
</footer>