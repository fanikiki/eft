@extends('body') @section('title', 'Корзина') @section('centerbox')


<section class="content">
  <!-- begin breadcrumbs -->
  @include('partials.breadcrumbs', ['title' => 'Корзина', 'items' => ['Корзина' => '']])
  <!-- end breadcrumbs -->
  <div class="container">
    <div class="index-content">
      <div class="container-fluid">
        <div class="row">
          
          <div class="cartCont">
            <ngcart-cart></ngcart-cart>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
@include('angular.addtocart')
<script type="text/ng-template" id="template/ngCart/cart.html">


  <div class="alert alert-warning" role="alert" ng-show="ngCart.getTotalItems() === 0">
      Your cart is empty
  </div>
  
  <div ng-show="ngCart.getTotalItems() > 0" >
  
    <table class="table table-desktop table-striped ngCart cart cart-check-table md-whiteframe-2dp">
      <tbody>
      <tr>
        <th></th>
        <th>Название товара</th>
        <th>Цена за единицу</th>
        <th>Количество</th>
        <th>Общая стоимость</th>
        <th>Удалить</th>
      </tr>

      <tr ng-repeat="item in ngCart.getCart().items track by $index">
          <td>
            <a href="@{{ item.getImage()}}" class="imgLightbox">
              <img src="@{{ item.getImage()}}" alt="">
            </a>
          </td>
          <td class="table-name">
            <div layout="column" layout-align="start start" layout-align-xs="start center">
              <a href="@{{ item.getLink() }}">@{{ item.getName() }}</a>
              <span>арт. @{{ item.getArticle() }}</span>
            </div>
          </td>
          <td class="table-price">@{{ item.getPrice()}} <i class="fa fa-rub" aria-hidden="true"></i></td>
          <td class="table-qty">
            <span class="ion-ios-arrow-left" 
                  ng-class="{'disabled':item.getQuantity()==1}"
                  ng-click="item.setQuantity(-1, true)"></span>&nbsp;&nbsp;
            <input ng-model="name"
                   onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                   maxlength="3"
                   ng-change="item.setQuantity(name, false)" 
                   ng-value="item.getQuantity()"
                   type="text" >
            <span class="ion-ios-arrow-right" ng-click="item.setQuantity(1, true)"></span>

            <input type="hidden" value="@{{ item.getQuantity() | number }}" name="data[@{{ item.getId() | number }}]">
            
          </td>
          <td class="table-total">@{{ item.getTotal()}} <i class="fa fa-rub" aria-hidden="true"></i></td>
          <td class="table-close">
            <md-button aria-label="delete-from-cart" class="md-raised" ng-click="ngCart.removeItemById(item.getId())">
              <span class="ion-close"></span>
            </md-button>
          </td>
      </tr>

      </tbody>
      <tfoot>
        <tr>
        <td></td>
        <td></td>
        <td></td>
        <td class="table-price-title">Общая стоимость заказа</td>
        <td class="table-price">@{{ ngCart.totalCost()}} <i class="fa fa-rub" aria-hidden="true"></i></td>
        <td></td>
        
        </tr>
      </tfoot>
  </table>

  <table class="table-mobile table table-striped ngCart cart cart-check-table md-whiteframe-2dp" >
    <tbody ng-repeat="item in ngCart.getCart().items track by $index">
      <tr>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td class="table-name">
          <div layout="column" layout-align="start start" layout-align-xs="start center">
              <a href="@{{ item.getLink() }}">@{{ item.getName() }}</a>
              <span>арт. @{{ item.getArticle() }}</span>
          </div>
        </td>
        <td class="table-close">
            <md-button aria-label="delete-from-cart" class="md-raised" ng-click="ngCart.removeItemById(item.getId())">
              <span class="ion-close"></span>
            </md-button>
          </td>
      </tr>
      <tr>
        <td colspan="2">
          <a href="@{{ item.getImage()}}" class="imgLightbox">
              <img src="@{{ item.getImage()}}" alt="">
          </a>
        </td>
       
      </tr>
      <tr>
        <td>Цена за единицу</td>
        <td class="table-price">@{{ item.getPrice()}} <i class="fa fa-rub" aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td>Количество</td>
        <td class="table-qty">
            <span class="ion-ios-arrow-left" 
                  ng-class="{'disabled':item.getQuantity()==1}"
                  ng-click="item.setQuantity(-1, true)"></span>&nbsp;&nbsp;
            <input ng-model="name"
                   onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                   maxlength="3"
                   ng-change="item.setQuantity(name, false)" 
                   ng-value="item.getQuantity()"
                   type="text" >
            <span class="ion-ios-arrow-right" ng-click="item.setQuantity(1, true)"></span>

            <input type="hidden" value="@{{ item.getQuantity() | number }}" name="data[@{{ item.getId() | number }}]">
            
          </td>
      </tr>
      <tr>
        <td>Общая стоимость</td>
        <td class="table-total">@{{ item.getTotal()}} <i class="fa fa-rub" aria-hidden="true"></i></td>
      </tr>
    </tbody>
    <tfoot>
     <tr>
      <td class="table-price-title">Общая стоимость заказа</td>
      <td class="table-price">@{{ ngCart.totalCost()}} <i class="fa fa-rub" aria-hidden="true"></i></td>
      </tr>
    </tfoot>
  </table>
  
 
    <div layout="row" layout-align="space-around start" class="cart-form" layout-wrap layout-align-xs="start stretch">
  
      <form flex-order-xs="2" id="cartform1" class=" cart-fast-form" role="form" method="post" action="/go" layout-align="start start" layout="column" name="cartFastForm" flex="45"  flex-sm="45" flex-xs="100">
      {!! Form::token() !!}

        <div class="cart-fast-check md-whiteframe-2dp">
          <h3>Ваши контактные данные</h3>
          
          <div class="form-group" >
                                <md-input-container class="md-block">
                                    <label>Ваше имя</label>
                                    <input class="form-control" ng-minlength="2" required name="regName" ng-model="reg.name">
                                    <div ng-messages="cartFastForm.regName.$error">
                                        <div ng-message="required">Это поле обязательное.</div>
                                        <div ng-message="minlength">Имя должно содержать не меньше 2 символов.</div>
                                    </div>
                                </md-input-container>
                            </div>
            <div class="form-group">
              <md-input-container class="md-block">
                                <label>Ваш Email</label>
                                <input class="form-control" name="email" ng-model="reg.authEmail" required minlength="4"
                                       ng-pattern="/^.+@.+\..+$/" type="email">
                               <div ng-messages="cartFastForm.email.$error" role="alert">                            
                                        <div ng-message="required">Это поле обязательное.</div>
                                        <div ng-message="minlength">Ваш email должен содержать не менее 4 символов и должен быть достоверным</div>
                                        <div ng-message="pattern">Email должен быть достоверным.</div>
                                </div>
                            </md-input-container>
            </div>
            <div class="form-group">
                                <md-input-container class="md-block">
                                    <label>Телефон</label>
                                    <input class="form-control" ng-pattern="/^[0-9 ]+$/" ng-minlength="4" type="text" required name="phone" ng-model="event.phone">
                                    <div ng-messages="cartFastForm.phone.$error" role="alert" multiple>
                                        <div ng-message="required">Это поле обязательное.</div>
                                        <div ng-message="minlength">Телефон долен содержать не меньше 4 символов.</div>
                                        <div ng-message="pattern">Телефон должно содержать только цифры.</div>
                                    </div>
                                </md-input-container>
            </div>
            
          
          <span>* - обязательные для заполнения поля</span>
        </div>
    
        <div class="cart-fast-person md-whiteframe-2dp">
          <h3>Я буду оформлять заказ как</h3>
          <div layout="row" layout-align="space-around start">
            
              <div class="form-group">
                                <input checked required="" type="radio" id="radio01" name="radio" />
                                <label for="radio01">
                                    <span >Частное лицо</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <input required="" type="radio" id="radio02" name="radio" />
                                <label for="radio02">
                                   <span >Юридическое лицо</span>
                                </label>
                            </div>
            
          </div>
        </div>
    
        <div class="cart-fast-delivery md-whiteframe-2dp">
          <h3>Где и как Вы хотите получить заказ</h3>
          <div layout="row" layout-align="space-around start">
          <div class="form-group">
                                <input checked required="" type="radio" id="radio03" name="radio1" />
                                <label for="radio03">
                                    <span>Самовывоз</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <input required="" type="radio" id="radio04" name="radio1" />
                                <label for="radio04">
                                   <span >Доставка</span>
                                </label>
                            </div>
                            </div>
        </div>
        
    
        <md-button type="submit" class="md-raised  cart-fast-submit">Быстрый заказ</md-button>
      </form>
  
      <div flex-order-xs="1" flex="40" flex-sm="45" flex-xs="100" layout="column" layout-align="start stretch">

          <div class="cart-auth md-whiteframe-2dp" >
        
              <div layout="row" layout-align="space-between center">
                <span>Уже покупали у нас ?</span>
                <md-button md-ink-ripple="false" class="button md-raised" aria-label="description" data-remodal-target="loginForm">Авторизируйтесь</md-button>
              </div>
              <div layout="row" layout-align="space-between center">
                <span>Покупаете первый раз ?</span>
                <md-button md-ink-ripple="false" class="button md-raised" aria-label="description" data-remodal-target="regForm">Зарегистрируйтесь</md-button>
              </div>

          </div>
      
           <div class="cart-auth-alert md-whiteframe-2dp">
                <span>
                  Получайте бонусы за покупки. <br>
                  Отслеживайте состояние своего заказа и смотрите историю заказов.
                </span>
            </div>

      </div>
      
  </div>
 
</div>


<script>
$('#cartform1').formValidation({
                    fields: {
                        phone: { 
            validators: {
                notEmpty: {}, 
                stringLength: { min: 4, }, 
                regexp: { regexp: '[0-9 ]+', } 
            } 
        }, 
        email: { 
            validators: { 
                notEmpty: {}, 
                stringLength: { min: 4, }, 
                emailAddress: {}, 
                regexp: { regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$', } 
            } 
        }, 
        regName: { 
            validators: { 
                notEmpty: {}, 
                stringLength: { min: 2, } 
            } 
        }, 
                    }
                })
.on('success.form.fv', function(e) {
                    // Prevent form submission
                    e.preventDefault();
        
                    var $form = $(e.target),
                        formData = new FormData(),
                        params   = $form.serializeArray(),
                        fv    = $(e.target).data('formValidation');
                        
                    $.each(params, function(i, val) {
                        formData.append(val.name, val.value);
                    });
                    
                    $.ajax({
                            url: $form.attr('action'),
                            data: formData,
                            async: true,
                            cache: false,
                            contentType: false,
                            processData: false,
                            type: 'POST',
                            success: function(result) {
                                swal("Поздравляем", "Ваше сообщение отправлено", "success");
                                $(".trigger-cart-clear").click()
                            }   
                        });
                });

 
 $(document).ready(function() {


    $('.imgLightbox').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });
  });

</script>

</script>

@stop