@extends('body')
@include('partials.meta', ['data' => $data['model'], 'title' => $data['model']->name])
@section('centerbox')

    @include('products.partials.modals', ['product' => $data['model']->name])

    <!-- BREADCRUMBS -->
    <ol class="breadcrumb">
        <li><a href="{{ route('index') }}">Главная</a></li>
        <li><a href="{{ route('get-category', $data['category']->slug) }}">{{$data['category']->name}}</a></li>
        <li class="active">{{$data['model']->name}}</li>
    </ol>
    <section class="page-section breadcrumbs text-right product">
            <div class="page-header">
                <h1>{{$data['model']->name}}</h1>
            </div>
            <script type="application/ld+json">
            {
             "@context": "http://schema.org",
             "@type": "BreadcrumbList",
             "itemListElement":
             [
              {
               "@type": "ListItem",
               "position": 1,
               "item":
               {
                "@id": "{{ route('index') }}",
                "name": "Главная"
                }
              },
              {
               "@type": "ListItem",
               "position": 2,
               "item":
               {
                "@id": "{{ route('get-category', $data['category']->slug) }}",
                "name": "{{$data['category']->name}}"
                }
              },
              {
               "@type": "ListItem",
               "position": 3,
               "item":
               {
                "name": "@yield('title')"
                }
              }
             ]
            }
            </script>

    </section>
    <!-- /BREADCRUMBS -->

    <!-- PAGE WITH SIDEBAR -->
    <section class="page-section sub-page">
            <div class="row">
                <!-- CONTENT -->
                <div class="col-md-12" id="content">
                    <div class="car-big-card alt">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="owl-carousel img-carousel">
                                    @if(isset($data['model']->photos))
                                    @foreach($data['model']->photos as $photo)
                                    <div class="item">
                                        <a class="btn btn-zoom" href="uploaded/{{$photo->source}}" data-gal="prettyPhoto"><i class="fa fa-arrows-h"></i></a><a href="uploaded/{{$photo->source}}" data-gal="prettyPhoto"><img class="img-responsive" src="uploaded/{{$photo->source}}" alt="{{$data['model']->product_text}}"/></a>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <div class="row car-thumbnails">
                                    @if(isset($data['model']->photos))
                                        @foreach($data['model']->photos as $k => $thumb)
                                            <div class="col-xs-2 col-sm-2 col-md-3 model-thumb"><a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [{{$k}}, 300, true]);"><img src="uploaded/{{$thumb->source}}" alt="{{$data['model']->product_text}}"/></a></div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="page-divider half transparent"/>
                    <h3>{{$data['model']->price}} руб.</h3>
                    <h3 class="block-title alt collapsed" data-toggle="collapse" data-target="#product-desc">Описание</h3>
                    <div id="product-desc" class="collapse">{!! $data['model']->description !!}</div>

                    <h3 class="block-title alt collapsed" data-toggle="collapse" data-target="#product-specs">
                        Характеристики</h3>
                    <div class="collapse" id="product-specs">
                       {!! $data['model']->description_short !!}
                    </div>
                    @if(count($data['model']->products) > 0)
                        <h3 class="block-title alt" data-toggle="collapse" data-target="#product-package">
                            Комплектность</h3>
                        <div class="collapse in" id="product-package">
                            <div class="funkyradio">
                                @foreach($data['model']->products->sortBy('sort') as $product)
                                    <div class="funkyradio-danger">
                                        <input type="radio" name="radio" id="package-{{$product->id}}" class="productpackage"
                                               @if(isset($product->photos{0}))
                                               data-photo="{{$product->photos{0}->source}}"
                                               @endif
                                               data-price="{{$product->price}}"
                                               data-package-id="{{$product->id}}"
                                               data-description="{{$product->description}}"/>
                                        <label for="package-{{$product->id}}">{{$product->product_number}} <span class="package-price">{{$product->price}} руб.</span></label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12 no-print">
                        <div class="col-md-4">
                            <button class="btn btn-default btn-block model-test-drive">Тест-драйв</button>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-default btn-block model-demo">Демо-показ</button>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-default btn-block model-consultation">Заказать консультацию</button>
                        </div>
                    </div>
                </div>
                <!-- /CONTENT -->
            </div>

    </section>
    <!-- /PAGE WITH SIDEBAR -->
@endsection
@section('scripts')
<script src="js/plugins/printThis.js"></script>
<script>
    $(function () {

        $(".print-package").click(function (e) {
            e.preventDefault();

            $("#product-modal").find(".modal-body").printThis();
        });

        $(".model-test-drive").click(function () {
            $("#product-testdrive").modal('show');
        });

        $(".model-demo").click(function () {
            $("#product-demo").modal('show');
        });

        $(".model-consultation").click(function () {
            $("#product-consultation").modal('show');
        });

        $(".test-drive-frm").validate({
            rules:{
                product: {required: true},
                fio: {required: true},
                company: {required: true},
                phone: {required: true},
                email: {
                    required: true,
                    email: true
                }
            },
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function (form) {
                $.ajax({
                    method: "POST",
                    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                    url: $(form).attr("action"),
                    data: $(form).serialize(),
                    success: function () {
                        $("#product-testdrive").modal('hide');

                        swal({
                            type: 'success',
                            title: 'Спасибо! Ваша заявка отправлена',
                            showConfirmButton: false,
                            timer: 5000
                        });

                        $(form).trigger('reset');
                    }
                });

                return false;
            }
        });

        $(".demo-frm").validate({
            rules:{
                d_fio: {required: true},
                d_company: {required: true},
                d_phone: {required: true}
            },
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function (form) {
                $.ajax({
                    method: "POST",
                    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                    url: $(form).attr("action"),
                    data: $(form).serialize(),
                    success: function () {

                        $("#product-demo").modal('hide');

                        swal({
                            type: 'success',
                            title: 'Спасибо! Ваша заявка отправлена',
                            showConfirmButton: false,
                            timer: 5000
                        });

                        $(form).trigger('reset');
                    }
                });

                return false;
            }
        });

        $(".consultation-frm").validate({
            rules:{
                d_fio: {required: true},
                d_company: {required: true},
                d_phone: {required: true}
            },
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function (form) {
                $.ajax({
                    method: "POST",
                    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                    url: $(form).attr("action"),
                    data: $(form).serialize(),
                    success: function () {

                        $("#product-consultation").modal('hide');

                        swal({
                            type: 'success',
                            title: 'Спасибо! Ваша заявка отправлена',
                            showConfirmButton: false,
                            timer: 5000
                        });

                        $(form).trigger('reset');
                    }
                });

                return false;
            }
        });
    });
</script>
@endsection