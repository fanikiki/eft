<!-- PACKAGE MODAL -->
<div class="modal fade" tabindex="-1" role="dialog" id="product-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="orange modal-title">Информация о комплекте</div>
      </div>
      <div class="modal-body">
        <img src="" class="img-responsive center-block" width="250px" id="product-image" alt="Информация о комплекте">
        <div id="product-description"></div>
        {{--<p>Состав комплекта:</p>
        <ul class="list-group">
          <li class="list-group-item">EFT M3 GNSS <span class="badge">1 шт.</span></li>
          <li class="list-group-item">Антенна ASH-661 L1+L2 <span class="badge">2 шт.</span></li>
          <li class="list-group-item">USB кабель <span class="badge">3 шт.</span></li>
        </ul>--}}
        <p class="font-big orange">
            Цена: <span id="product-price">20000</span> руб.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left print-package">Печать</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- PACKAGE MODAL -->

<!-- TESTDRIVE MODAL -->
<div class="modal fade" tabindex="-1" role="dialog" id="product-testdrive">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="orange modal-title">Тест-драйв</div>
            </div>
            <div class="modal-body">
                <div class="test-drive-container">
                    {!! $testdrive_page->description !!}

                    <form role="form" method="post" action="{{ route('send-test-drive') }}" class="test-drive-frm">
                        <div class="form-group">
                            <label for="testdrive_fio">ФИО*</label>
                            <input type="text" class="form-control" name="fio" id="fio" placeholder="ФИО">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_company">Название компании*</label>
                            <input type="text" class="form-control" name="company" id="company" placeholder="Название компании">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_phone">Телефон*</label>
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Телефон">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_message">Комментарий</label>
                            <textarea class="form-control" name="comment" id="comment" placeholder="Комментарий"></textarea>
                        </div>
                        <button type="submit" class="btn btn-theme btn-testdrive"><span class="glyphicon glyphicon-fire"></span> Заказать тест-драйв!</button>
                        <input type="hidden" name="product" value="{{$product}}">
                        <input type="hidden" name="g-recaptcha-response" class="g-recaptcha">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- TESTDRIVE MODAL -->

<!-- DEMO MODAL -->
<div class="modal fade" tabindex="-1" role="dialog" id="product-demo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="orange modal-title">Демо-показ</div>
            </div>
            <div class="modal-body">
                <div class="test-drive-container">

                    {!! $demo_page->description !!}

                    <form role="form" method="post" action="{{ route('send-demo') }}" class="demo-frm">
                        <div class="form-group">
                            <label for="testdrive_fio">ФИО*</label>
                            <input type="text" class="form-control" name="d_fio" id="d-fio" placeholder="ФИО">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_company">Название компании*</label>
                            <input type="text" class="form-control" name="d_company" id="d-company" placeholder="Название компании">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_phone">Телефон*</label>
                            <input type="text" class="form-control" name="d_phone" id="d-phone" placeholder="Телефон">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_message">Комментарий</label>
                            <textarea class="form-control" name="d_comment" id="d-comment" placeholder="Комментарий"></textarea>
                        </div>
                        <input type="hidden" name="d_product" value="{{$product}}">
                        <input type="hidden" name="g-recaptcha-response" class="g-recaptcha">
                        <button type="submit" class="btn btn-theme btn-demo">Заказать демо-показ!</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- DEMO MODAL -->

<!-- ORDER MODAL -->
<div class="modal fade" tabindex="-1" role="dialog" id="product-order">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="orange modal-title">Заказать</div>
            </div>
            <div class="modal-body">
                <div class="test-drive-container">
                    <form role="form" method="post" action="{{ route('send-demo') }}" class="demo-frm">
                        <div class="form-group">
                            <label for="testdrive_fio">ФИО*</label>
                            <input type="text" class="form-control" name="d_fio" id="d-fio" placeholder="ФИО">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_company">Название компании*</label>
                            <input type="text" class="form-control" name="d_company" id="d-company" placeholder="Название компании">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_phone">Телефон*</label>
                            <input type="text" class="form-control" name="d_phone" id="d-phone" placeholder="Телефон">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_message">Комментарий</label>
                            <textarea class="form-control" name="d_comment" id="d-comment" placeholder="Комментарий"></textarea>
                        </div>
                        <input type="hidden" name="g-recaptcha-response" class="g-recaptcha">
                        <button type="submit" class="btn btn-theme btn-demo">Заказать</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ORDER MODAL -->

<!-- CONSULTATION MODAL -->
<div class="modal fade" tabindex="-1" role="dialog" id="product-consultation">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="orange modal-title">Консультация</div>
            </div>
            <div class="modal-body">
                <div class="test-drive-container">
                    <form role="form" method="post" action="{{ route('send-consultation') }}" class="consultation-frm">
                        <div class="form-group">
                            <label for="testdrive_fio">ФИО*</label>
                            <input type="text" class="form-control" name="d_fio" id="d-fio" placeholder="ФИО">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_company">Название компании*</label>
                            <input type="text" class="form-control" name="d_company" id="d-company" placeholder="Название компании">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_phone">Телефон*</label>
                            <input type="text" class="form-control" name="d_phone" id="d-phone" placeholder="Телефон">
                        </div>
                        <div class="form-group">
                            <label for="testdrive_message">Комментарий</label>
                            <textarea class="form-control" name="d_comment" id="d-comment" placeholder="Комментарий"></textarea>
                        </div>
                        <input type="hidden" name="product" value="{{$product}}">
                        <input type="hidden" name="g-recaptcha-response" class="g-recaptcha">
                        <button type="submit" class="btn btn-theme btn-demo">Заказать консультацию!</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->