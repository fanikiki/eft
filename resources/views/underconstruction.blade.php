@extends('body') @section('title', 'В разработке') @section('centerbox')
<section class="content">
  <!-- begin breadcrumbs -->
  @include('partials.breadcrumbs', ['title' => 'В разработке', 'items' => ['Скоро...' => '']])
  <!-- end breadcrumbs -->
  <div class="container">
    <div class="index-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-9 col-xs-12 ">
            <h2>Страница в разработке</h2>
          </div>
          @include('partials.sidebar')
        </div>
      </div>
    </div>
  </div>
</section>
@stop