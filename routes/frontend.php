<?php

/*
 *
 * FRONTEND
 *
 */
Route::group(['middleware' => ['shared']], function () {
    Route::get('/', [
        'as'    => 'index',
        'uses'  => 'HomeController@index'
    ]);

    Route::post('getmodels', 'HomeController@getModels');

    Route::post('getproducts', 'HomeController@getProducts');

    Route::post('getmanufacturers', 'HomeController@getManufacturers');

    /*Route::get('catalog', ['uses'  => 'HomeController@getCatalog', 'as' => 'catalog']);*/

    //Временно
    /*Route::get('product', ['uses'  => 'HomeController@getProduct', 'as' => 'product']);*/

    /*Route::get('terms', ['uses'  => 'HomeController@getTerms', 'as' => 'terms']);*/

    /*Route::get('about', ['uses'  => 'HomeController@getAbout', 'as' => 'about']);*/

    Route::get('contacts', ['uses'  => 'HomeController@getContacts', 'as' => 'contacts']);

    Route::get('catalog/{category_slug}', ['uses' => 'ProductsController@getCategory', 'as' => 'get-category']);

    Route::get('catalog/{category_slug}/{model_slug}', ['uses'  => 'ProductsController@getModel', 'as' => 'get-model']);

    Route::get('news', ['uses' => 'NewsController@getNewsList', 'as' => 'news']);

    Route::get('news/{slug}', ['uses' => 'NewsController@getNews', 'as' => 'get-news']);

    Route::get('test-drive', ['uses' => 'HomeController@testDrive', 'as' => 'test-drive']);
    Route::post('send-test-drive', ['uses' => 'MailController@sendTestdrive', 'as' => 'send-test-drive']);
    Route::get('demo', ['uses' => 'HomeController@demo', 'as' => 'demo']);
    Route::post('send-demo', ['uses' => 'MailController@sendDemo', 'as' => 'send-demo']);
    Route::get('trade-in', ['uses' => 'HomeController@tradeIn', 'as' => 'trade-in']);
    Route::post('send-trade-in', ['uses' => 'MailController@sendTradeIn', 'as' => 'send-trade-in']);

    Route::post('newsletter-subscribe', ['uses'=>'MailController@newsletterSubscribe', 'as'=>'subscribe']);

    Route::post('send-contactform', ['uses'=>'MailController@sendContactForm', 'as'=>'send-contactform']);

    Route::post('send-consultation', ['uses' => 'MailController@sendConsultation', 'as' => 'send-consultation']);

    Route::post('send-order-calc', ['uses'=>'MailController@sendOrderCalc', 'as'=>'send-order-calc']);

    Route::post('send-order-model', ['uses'=>'MailController@sendOrderModel', 'as'=>'send-order-model']);

    Route::get('{slug}', ['uses'=>'ContentController@getBySlug', 'as'=>'content']);
});